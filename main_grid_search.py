import getpass

import numpy as np
from sklearn.tree import DecisionTreeClassifier

from config import Config, GridSearchMode, ClassifierType
from model_selection.grid_search import GridSearch
from util.log import Log
from util.time import Stopwatch
from util.util import send_mail

TAG = __name__

cfg = Config('config.yaml')


def get_grid_params(grid_type):
    params = {}

    if grid_type == GridSearchMode.ALL or grid_type == GridSearchMode.CLF:

        t = cfg.get_clf_type()
        if t is ClassifierType.FM:
            params.update(
                dict(
                    init_stdev=[0.05, 0.01, 0.005, 0.001],
                    k0=[0, 1],
                    k1=[1],
                    k2=range(2, 8),
                    num_iter=[50, 100, 150]))
        elif t is ClassifierType.Ada:
            params.update(
                dict(
                    base_estimator=[DecisionTreeClassifier(), DecisionTreeClassifier(max_depth=1)],
                    algorithm=['SAMME', 'SAMME.R'],
                    n_estimators=[10, 25, 50, 100, 250, 500, 750, 1000],
                    learning_rate=[0.1, 0.25, 0.5, 0.75, 1.0]))
        elif t is ClassifierType.Bagging:
            params.update(
                dict(
                    base_estimator=[DecisionTreeClassifier(), DecisionTreeClassifier(max_depth=1)],
                    n_estimators=[10, 25, 50, 100, 250, 500, 750, 1000],
                    max_samples=[0.5, 0.75, 0.875, 1.0],
                    max_features=[0.5, 0.75, 0.875, 1.0],
                    bootstrap=[True, False],
                    bootstrap_features=[False, True]))

        elif t is ClassifierType.SVM:
            # params taken from M. Illecker's SentiStorm thesis: http://diglib.uibk.ac.at/ulbtirolhs/download/pdf/423072
            params.update(
                dict(
                    C=[2 ** x for x in range(-5, 16, 2)],
                    gamma=[2 ** x for x in range(-15, 4, 2)],
                    kernel=['linear', 'poly', 'rbf']))
        else:
            Log.e(TAG, "Invalid ClassifierType {}. Exiting...".format(t))
            exit()

        # if grid_type == GridSearchMode.ALL:  # add pipeline prefix
        params = {cfg.get('clf', 'use') + '__' + k: v for k, v in params.items()}

    if grid_type == GridSearchMode.ALL or grid_type == GridSearchMode.FEATURE:
        params.update(dict(
            pre__active=[True, False],
            # union__emoticon__active=[True, False],
            # union__lexicons__active=[True, False],
            # union__lexicons__union__afinn__active=[True, False],
            # union__lexicons__union__maxDiff__active=[True, False],
            # union__lexicons__union__opinion__active=[True, False],
            # union__lexicons__union__sStrength__active=[True, False],
            # union__lexicons__union__sent140__active=[True, False],
            # union__lexicons__union__swn__active=[True, False],
            # union__punct__active=[True, False],
            # union__text_vect__active=[True, False],
            # union__pos__active=[True, False],
            union__text_vect__vect__vect__stop_words=[None, 'english'],
            union__text_vect__vect__vect__binary=[True, False],
            union__text_vect__vect__vect__ngram_range=[(1, 1), (1, 2), (1, 3)],
            union__text_vect__vect__vect__max_features=[None, 5000, 10000, 15000],
            union__text_vect__vect__vect__min_df=[1, 5, 10, 50],
            union__text_vect__vect__vect__max_df=[0.7, 0.8, 0.9, 1.0],
            selection__k=['all', 5000, 10000, 15000]))
    return params


if __name__ == "__main__":

    email_settings = cfg.get('email')
    grid_settings = cfg.get('gridsearch')
    sampler = cfg.get_sampler()
    grid_type = GridSearchMode[grid_settings.get('type')]
    vader_enabled = cfg.is_feature_enabled('vader')
    use_cv = grid_settings.get('folds')

    from_ = email_settings['from']
    to_ = email_settings['to']
    smtp_ = email_settings['smtp']
    port_ = email_settings['port']
    login_ = email_settings['login']
    mail_pw = getpass.getpass("Email password for '{}' or Enter to skip email notification: "
                              .format(email_settings['login']))

    if mail_pw is "":
        Log.d(TAG, "No password specified. Won't send notification email after grid search finishes.")
        mail_pw = None
    else:
        body = 'GridSearch for {} over {} parameters started'.format(
            cfg.get_clf_name(), grid_type)
        send_mail("GridSearch started", body, from_, to_, smtp_, port_, login_, mail_pw)

    stop = Stopwatch(start=False)
    stop.start('Reading Tweets')
    X_train, y_train, X_test, y_test = cfg.get_tweets()
    stop.stop()

    # use both sets for train/test split
    if use_cv > 1:
        X_train = X_train + X_test
        y_train = np.concatenate((y_train, y_test))

        X_test = None
        y_test = None

    pipeline = cfg.build_pipeline()

    params = get_grid_params(grid_type)

    if grid_type == GridSearchMode.ALL:
        Log.d(TAG, "Search space over whole pipeline")
    elif grid_type == GridSearchMode.CLF:
        Log.d(TAG, "Search space only for classifier parameters")
    elif grid_type == GridSearchMode.FEATURE:
        Log.d(TAG, "Search space only for feature parameters")
    else:
        Log.e(TAG, "Unknown GridSearchMode")

    stop.restart('Gridsearch')

    gs = GridSearch(pipeline, params,
                    jobs=grid_settings['jobs'], n_folds=use_cv, sampler=sampler, verbose=5,
                    results_file=grid_settings['result'], overwrite=grid_settings['overwrite'])
    gs.run(X_train, y_train, X_test, y_test)  # if using CV, test data = None
    elapsed_time = stop.stop()

    Log.d(TAG, 'F1 p/n\t{}'.format(gs.sprint_result(gs.best_f1_pn(), sort=True)))
    Log.d(TAG, 'F1\t\t{}'.format(gs.sprint_result(gs.best_f1(), sort=True)))
    Log.d(TAG, 'Acc\t\t{}'.format(gs.sprint_result(gs.best_acc(), sort=True)))

    if mail_pw:
        subject = 'GridSearch completed'
        body = 'GridSearch for {} over {} parameters finished after {}'.format(
            cfg.get_clf_name(), grid_type, elapsed_time)

        send_mail(subject, body, from_, to_, smtp_, port_, login_, mail_pw)
