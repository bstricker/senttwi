from pprint import pprint

import numpy as np

from config import Config
from evaluate import evaluate
from model_selection.cv import KFoldCV
from util.time import Stopwatch

if __name__ == '__main__':
    config = Config('config.yaml')

    X_train, y_train, X_test, y_test = config.get_tweets()
    X = X_train + X_test
    y = np.concatenate((y_train, y_test))

    sampler = config.get_sampler()
    pipeline = config.build_pipeline()

    cv = KFoldCV(pipeline, X, y, n_folds=5, jobs=-2, sampler=sampler)

    stop = Stopwatch(msg="Start CV")

    pprint(cv.cross_val_score(mapping=evaluate.Mapping.three_point_map, averaging=True))

    stop.stop()
