from sklearn.base import BaseEstimator

from libs.pywFM import pywFM
from util.error import MissingArgumentError

"""
Wrapper around the pywFM classes.

This wrapper is necessary to have two separate classes for Regression and Classification
because in pywFM the parameter task is mandatory, whereas gridsearch needs a constructor
without mandatory parameters.

This wrapper can also be used to rename pywFM's parameters to match with other Classifiers
(e.g. fastFM) parameters.

Important note: This is just an attempt to build a wrapper for the pywFM to conform with
scikit learn's estimator interfaces (fit, predict, ...)

"""


class BaseFM(pywFM.FM, BaseEstimator):
    def __init__(self, task, num_iter=100, init_stdev=0.1, k0=True, k1=True, k2=8, learning_method='mcmc',
                 learn_rate=0.1, r0_regularization=0, r1_regularization=0, r2_regularization=0, rlog=True,
                 verbose=False, silent=False, temp_path=None):

        super().__init__(task, num_iter, init_stdev, k0, k1, k2, learning_method, learn_rate, r0_regularization,
                         r1_regularization, r2_regularization, rlog, verbose, silent, temp_path)

        # data for fit and predict
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None

    def fit(self, X, y, y_test):
        """
        Fit Factorization Machine.

        Note: In this case fitting does only save the data for later prediction.

        :param X: training data
        :param y: training labels
        :param y_test: test labels
        :return: self
        """

        if y is None:
            raise MissingArgumentError('y', message='y is necessary to perform predict')

        if y_test is None:
            raise MissingArgumentError('y_test', message='y_test is necessary to perform predict')

        # save data for later predicting
        self.x_train = X
        self.y_train = y
        self.y_test = y_test

        return self

    def predict(self, X):
        if self.y_test is None:
            raise RuntimeError('Calling predict without calling fit first!')

        self.x_test = X

        # now run libFM with pywFM
        model = self.run(self.x_train, self.y_train, self.x_test, self.y_test)

        return model.predictions

    def set_params(self, **params):
        super(BaseFM, self).set_params(**params)


class FMRegression(BaseFM):
    def __init__(self, num_iter=100, init_stdev=0.1, k0=True, k1=True, k2=8, learning_method='mcmc',
                 learn_rate=0.1, r0_regularization=0, r1_regularization=0, r2_regularization=0, rlog=True,
                 verbose=False, silent=False, temp_path=None):
        # BaseFM constructor
        super().__init__('regression', num_iter=num_iter, init_stdev=init_stdev, k0=k0, k1=k1, k2=k2,
                         learning_method=learning_method, learn_rate=learn_rate, r0_regularization=r0_regularization,
                         r1_regularization=r1_regularization, r2_regularization=r2_regularization, rlog=rlog,
                         verbose=verbose, silent=silent, temp_path=temp_path)


class FMClassification(BaseFM):
    def __init__(self, num_iter=100, init_stdev=0.1, k0=True, k1=True, k2=8, learning_method='mcmc',
                 learn_rate=0.1, r0_regularization=0, r1_regularization=0, r2_regularization=0, rlog=True,
                 verbose=False, silent=False, temp_path=None):
        # BaseFM constructor
        super().__init__('classification', num_iter=num_iter, init_stdev=init_stdev, k0=k0, k1=k1, k2=k2,
                         learning_method=learning_method, learn_rate=learn_rate, r0_regularization=r0_regularization,
                         r1_regularization=r1_regularization, r2_regularization=r2_regularization, rlog=rlog,
                         verbose=verbose, silent=silent, temp_path=temp_path)
