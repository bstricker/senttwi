import re

from nltk.stem.snowball import EnglishStemmer
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS

from data.emoticons import is_emoticon
from util import util


class BasePostProcessor(object):
    def __call__(self, tokens):
        raise NotImplementedError("Use of base class not allowed!")

    def __str__(self):
        """
        Return class name.
        """
        return self.__class__.__name__

    def __repr__(self):
        """ Return signature."""
        return self.__class__.__name__

    def __eq__(self, other):
        if other is None:
            return False
        return self.__dict__ == other.__dict__


class StemmingPostProcessor(BasePostProcessor):
    def __init__(self, stemmer=EnglishStemmer()):
        self.stemmer = stemmer

    def __call__(self, tokens):
        return [self.stemmer.stem(token) for token in tokens]

    def __repr__(self):
        return "{}('stemmer'={})".format(super().__repr__(), self.stemmer)


class StopwordPostProcessor(BasePostProcessor):
    TAG = 'StopwordPostProcessor'

    def __init__(self, stopwords=None):
        """
        Remove stopwords in a given list of tokens.

        :param stopwords: list: user-defined list of stopwords, None uses sklearn's ENGLISH_STOP_WORDS
        :param key: optional,
                    function to extract the word to compare with the stopword from each list element (e.g. itemgetter(0))

        """
        if not stopwords:
            self.stopwords = ENGLISH_STOP_WORDS

    def __call__(self, tokens):
        return [token for token in tokens if token not in self.stopwords]


class LowercasePostProcessor(BasePostProcessor):
    def __init__(self):
        """Lowercase all words in a given list of tokens."""

    def __call__(self, tokens):
        return [token.lower() for token in tokens]


class RepeatingPostProcessor(BasePostProcessor):
    """
    Collapse groups of consecutive characters in a list of tweets.
    
    Uses a two step approach:
    1) Substitute two or more occurrences of the same special characters into a single character.
    2) Substitute three or more occurrences of the same alpha-numeric character into two same characters.
    
    Note
    ----
    Special Characters are all non word characters according to the \W regex character class.
    
    Example
    -------
    >>>  t = ["Hello Wooooorld !!!!!", "aaaaaah", ":-))", "!!!!!......!!!"]
    >>>  RepeatingPostProcessor()(t)
    >>>  ['Hello Woorld !', 'aah', ':-)', '!.!']
    """

    def __init__(self):
        self.re_special = re.compile(r'(\W)\1+')
        self.re_all = re.compile(r'(.)\1{2,}')

    def __call__(self, tokens):
        tokens = [self.re_special.sub(r'\1', token) for token in tokens]
        return [self.re_all.sub(r'\1\1', token) for token in tokens]


class PosPostProcessor(BasePostProcessor):
    TAG = 'PosPostProcessor'

    def __init__(self, remove_url=False, remove_punct=False, lowercase=True, remove_stop=False):
        """
        Allows to specify some rules to prune the token list.

        :param remove_url: remove urls and email addresses from token list
        :param remove_punct: remove punctuation tokens (e.g. '.' , '?' , '!' , '...' )
        """
        self.remove_url = remove_url
        self.remove_punct = remove_punct
        self.lowercase = lowercase
        self.remove_stop = remove_stop

    def __call__(self, tagged_tweet):

        pruned = []
        for pos_tuple in tagged_tweet:
            token, tag, confidence = pos_tuple
            if tag == 'U':
                if self.remove_url:
                    continue
            elif tag == ',':
                if self.remove_punct:
                    continue
            if self.lowercase:
                token = token.lower()
            if self.remove_stop:
                if token in ENGLISH_STOP_WORDS:
                    continue
            pruned.append((token, tag, confidence))
        return pruned

    def __repr__(self):
        return "{}('remove_url'={}, 'remove_punct'={}, " \
               "'lowercase'={}, 'remove_stop'={})".format(super().__repr__(), self.remove_url, self.remove_punct,
                                                          self.lowercase, self.remove_stop)


class PrunePostProcessor(BasePostProcessor):
    TAG = 'PrunePostProcessor'

    def __init__(self, min_len=1, strip_hash=False, strip_mention=False, remove_url=False, remove_punctuation=False,
                 remove_numeric=False):
        """

        Allows to specify some rules to prune the token list.

        :param strip_hash: remove # symbol of hashtag and keep processing remaining string
        :param strip_mention: remove @ symbol from mention and keep processing remaining string
        :param remove_url: remove urls from token list
        :param remove_punctuation: remove punctuation-only one-letter tokens (e.g. '.' , '?' , '!' )
        :param remove_numeric: remove all tokens which contain no letters
        """
        # don't mess around with illegal lengths
        if min_len < 1:
            min_len = 1
        self.min_len = min_len
        self.strip_hash = strip_hash
        self.strip_mention = strip_mention
        self.remove_url = remove_url
        self.remove_punctuation = remove_punctuation
        self.remove_numeric = remove_numeric

    def __call__(self, tokens):

        pruned = []
        for token in tokens:
            if token.startswith('#'):
                if self.strip_hash:
                    # token = token[1:]
                    token = "#tag"
                    # continue
            if token.startswith('@'):
                if self.strip_mention:
                    # token = token[1:]
                    token = "#mention"
                    # continue
            if token.startswith('http'):
                if self.remove_url:
                    token = "#url"
                    # continue

            # remove all punctuation chars at the beginning and end of a string
            if self.remove_punctuation and not is_emoticon(token):
                while token.startswith(tuple(util.punctuation)):
                    token = token[1:]

                while token.endswith(tuple(util.punctuation)):
                    token = token[:-1]

            # separate if to check for single character hashtags and mentions (e.g. #1)
            if not re.search('[a-zA-Z]', token):
                if self.remove_numeric:
                    continue

            # check for min length
            if len(token) >= self.min_len:
                pruned.append(token)

        return pruned

    def __repr__(self):
        return "{}('min_len'={}, 'strip_hash'={}, 'strip_mention'={}, 'remove_url'={}, 'remove_punctuation'={}, " \
               "'remove_numeric'={})".format(super().__repr__(), self.min_len, self.strip_hash, self.strip_mention,
                                             self.remove_url, self.remove_punctuation, self.remove_numeric)
