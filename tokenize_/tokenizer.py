from libs import CMUTweetTagger, twokenize
from tokenize_.postprocessor import BasePostProcessor

TYPE_MULTI_LINE = 'multi-line'
TYPE_SINGLE_LINE = 'single-line'


class BaseTokenizer(object):
    """
    Base class for all self-written tokenizer.

    :param type_: if derived tokenizer tokenizes tweets tweet-by-tweet (single-line) or all at one (multi-line)
    :param postprocessor: optional callable or list of callables
                          perform one or more steps after tokenizing (e.g. removing or changing tokens)
                          steps will be performed in order they appear in the list and each output is the
                          input for the subsequent step

    """

    def __init__(self, type_, postprocessor=None):
        self.type_ = type_
        self.postprocessor = postprocessor

    def set_params(self, **params):
        """
        Set parameters if they exits.

        Simpler version of sklearn's BaseEstimator.set_params. Doesn't allow nested parameters and use a
        simple getattr check to validate if parameter exists, else raise ValueError.

        :param params: parameters to set
        :return: self
        """
        for key, value in params.items():

            # check if parameter exists
            try:
                getattr(self, key)
            except AttributeError:
                raise ValueError('Invalid parameter {} for tokenizer {}'.format(key, self.__class__.__name__))

            # simple objects case
            setattr(self, key, value)
        return self

    def __eq__(self, other):
        if other is None:
            return False
        return self.__dict__ == other.__dict__


class CmuPosTokenizer(BaseTokenizer):
    """
    Callable tokenizer to compute POS tags of a list of tweets.

    Uses a simple Python Wrapper [1] for CMU's ARK Tagger [2] which is written in Java.

    This tokenizer is a multi-line tokenizer because the performance is much better if the Java ARK Tagger is started
    once and fed with all tweets instead of computing each tweet separately (which would start a new
    Java Process for each tweet)

    After tokenizing, one or more  optional postprocessing steps can be done to refine the tag list.


    .. seealso::

    - [1] Python Wrapper            https://github.com/ianozsvald/ark-tweet-nlp-python/blob/master/CMUTweetTagger.py
    - [2] ARK Tagger                http://www.cs.cmu.edu/~ark/TweetNLP/
    - [3] POS Tagset (in Appendix)  http://www.cs.cmu.edu/~ark/TweetNLP/owoputi+etal.naacl13.pdf

    """

    TAG = 'CmuPosTokenizer'

    def __init__(self, postprocessor=None):
        super(CmuPosTokenizer, self).__init__(TYPE_MULTI_LINE, postprocessor)

    def __call__(self, docs):
        tagged = CMUTweetTagger.runtagger_parse(docs)

        if self.postprocessor:
            # single postprocessing step
            if isinstance(self.postprocessor, BasePostProcessor):
                tagged = [self.postprocessor(tags) for tags in tagged]
            else:
                # execute every postprocessing step in order they appear in the list
                for postproc in self.postprocessor:
                    tagged = [postproc(tags) for tags in tagged]

        return tagged


class TwokenTokenizer(BaseTokenizer):
    """
    Callable tokenizer to compute tokens of a list of a single tweet.

    Uses a Java Port [1] of CMU's ARK Tagger [2].

    Difference between this tokenizer and the CmuPosTokenizer is that CmuPosTokenizer also compute tags for each token.
    Although they (should) both produce the same tokens.

    After tokenizing, one or more  optional postprocessing steps can be done to refine the token list.

    .. seealso::

    - [1] Python Port Twokenizer    https://github.com/myleott/ark-twokenize-py
    - [2] ARK Tagger                http://www.cs.cmu.edu/~ark/TweetNLP/

    """

    def __init__(self, postprocessor=None):
        """
        Create Tokenizer.

        """
        super(TwokenTokenizer, self).__init__(TYPE_SINGLE_LINE, postprocessor)

    def __call__(self, doc):
        tokens = twokenize.tokenizeRawTweetText(doc)

        if self.postprocessor:
            # single postprocessing step
            if isinstance(self.postprocessor, BasePostProcessor):
                tokens = self.postprocessor(tokens)
            else:
                # execute every postprocessing step in order they appear in the list
                for postproc in self.postprocessor:
                    tokens = postproc(tokens)

        return tokens
