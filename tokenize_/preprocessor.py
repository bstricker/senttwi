import csv
import re

from sklearn.utils.validation import check_is_fitted, check_X_y

from pipeline import ActivatableComponent


class TranslationTable(object):
    """
    Translation table to substitute strings to other strings.

    The table consists of a mapping specifying the original term (key) and it's substitution (value).

    The table is a callable returns it's substitution when called with a string.

    If the specified mapping is a string, it has to be a file name pointing to a valid text file. The structure
    of this file has to be the following:

    >>> term1   substitution1
    >>> term2   substitution2

    Each pair has to be separated by a tab and in a single line.

    :param mapping: string or dict
        If string, mapping has to be an existing filename with the correct format.
        If dictionary, keys will be substituted by their values.
    """

    def __init__(self, mapping):
        super().__init__()
        if isinstance(mapping, str):
            self.mapping = self.read_mapping(mapping)
        else:
            self.mapping = mapping
        # join the keys into a big disjunction, eg. '(term1|term2|term3)'
        self.pattern = re.compile('(' + '|'.join(self.mapping) + ')')

    def __call__(self, string):
        # call regex's substitute method with own replace method
        return self.pattern.sub(self.replace, string)

    def replace(self, match):
        """
        Return substitution of the given match by looking up in the translation table.

        :param match: Match object given by the regex sub method
        :return: substitution of the matched string
        :rtype: str
        """
        token = match.group()
        sub = self.mapping[token]
        return sub

    @staticmethod
    def read_mapping(file_name):
        """
        Read the translation table from the given file name.

        The given text file must have the following structure:

        >>> term1   substitution1
        >>> term2   substitution2

        Each pair has to be separated by a tab and in a single line.

        :param file_name: name of the mapping file
        :return: dict of the mapping
        """
        mapping = {}
        with open(file_name, 'r') as file:
            reader = csv.reader(file, delimiter='\t')
            for row in reader:
                if not row or row[0][0] == '#':
                    continue
                mapping[row[0]] = row[1]

        return mapping


class SubstitutePreprocessor(ActivatableComponent):
    """
    Preprocessor substitute string in raw tweets.
    """

    def __init__(self, active=True, lower=True, negation=None, nominal_verb=None, slang=None):
        super().__init__(active)

        self.any = lower  # any step used?
        self.lower = lower
        self.negation = negation
        self.nominal_verb = nominal_verb
        self.slang = slang

    def fit(self, X=None, y=None):
        """Set up substitution mappings."""

        if not self.active:
            return self

        # initialize mappings
        self.negation_ = TranslationTable(self.negation) if self.negation else None
        self.nominal_verb_ = TranslationTable(self.nominal_verb) if self.nominal_verb else None
        self.slang_ = TranslationTable(self.slang) if self.slang else None

        return self

    def transform(self, X, y=None):
        """Transform each texts in a list into a simpler form by substituting abbreviations with their base form."""
        if not self.active:
            return X

        check_is_fitted(self, ['negation_', 'nominal_verb_', 'slang_'])

        tweets = []
        for tweet in X:

            if self.lower:
                tweet = tweet.lower()
            if self.negation:
                tweet = self.negation_(tweet)
            if self.nominal_verb:
                tweet = self.nominal_verb_(tweet)
            if self.slang:
                tweet = self.slang_(tweet)

            tweets.append(tweet)

        return tweets


if __name__ == '__main__':
    examples = ["Don't stop me now", "It's a long way to the top", "C'mon everybody!"]

    pre = SubstitutePreprocessor(negation='../data/map_negation.tsv',
                                 nominal_verb='../data/map_nominal.tsv',
                                 slang='../data/map_slang.tsv')
    examples = pre.fit_transform(examples)
    print('\n'.join(examples))
