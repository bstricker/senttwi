import numpy as np
from sklearn import metrics
from sklearn.metrics import accuracy_score, precision_score, \
    recall_score
from sklearn.utils import assert_all_finite

from util.log import Log

LABELS = [1, 0, -1]

TAG = __name__


def accuracy(y_true, y_pred, class_mapping=None, rounding=2):
    """
    Calculate accuracy (fraction of correct predictions).

    :param y_true: Ground truth (correct) target values.
    :param y_pred: Estimated target values.
    :param class_mapping: function to map regression values to classes, None if y_pred are classification values
    :param rounding: int or None, None omits rounding, positive integer specifies number of digits
    :return: accuracy
    """

    # no NaN or infinity allowed
    assert_all_finite(y_pred)

    if class_mapping is not None:
        y_pred = [class_mapping(x) for x in y_pred]

    acc = accuracy_score(y_true, y_pred)

    if rounding is not None:
        return round(acc.item(), rounding)

    return acc


def prec_recall_f1(y_true, y_pred, class_mapping=None, rounding=2):
    """
    Calculate precision, recall and f1 metric and return scores for each label (1, 0, -1).

    Example return tuple:
    (precision for each label, recall for each label, f1 for each label)
    ([0.6, 0.55, 0.6], [0.68, 0.7, 0.62], [0.65, 0.68, 0.6])

    :param y_true: Ground truth (correct) target values.
    :param y_pred: Estimated target values.
    :param class_mapping: function to map regression values to classes, None if y_pred are classification values
    :param rounding: int or None, None omits rounding, positive integer specifies number of digits
    :return: tuple of lists of scores for each label for each metric (precision, recall, f1)
    """
    # no NaN or infinity allowed
    assert_all_finite(y_pred)

    if class_mapping is not None:
        y_pred = Mapping.make_multiclass(y_pred, class_mapping)

    prec = precision_score(y_true, y_pred, average=None, labels=LABELS)
    recall = recall_score(y_true, y_pred, average=None, labels=LABELS)
    f1 = f1_score(y_true, y_pred, rounding=rounding)

    if rounding is not None:
        prec = np.round(prec, rounding)
        recall = np.round(recall, rounding)
        f1 = np.round(f1, rounding)

    return prec.tolist(), recall.tolist(), f1.tolist()


def f1_score(y_true, y_pred, class_mapping=None, rounding=2):
    # no NaN or infinity allowed
    assert_all_finite(y_pred)

    if class_mapping is not None:
        y_pred = Mapping.make_multiclass(y_pred, class_mapping)

    f1 = metrics.f1_score(y_true, y_pred, average=None, labels=LABELS)

    if rounding:
        f1 = np.round(f1, rounding)

    return f1.tolist()


def f1_pn_score(y_true, y_pred, class_mapping=None, rounding=2):
    f1 = f1_score(y_true, y_pred, class_mapping, rounding=0)
    return round((f1[0] + f1[2]) / 2, rounding)


def eval_all(y_true, y_pred, class_mapping=None, rounding=2):
    """
    Evaluate predictions with all scores.

    :param y_true: Ground truth (correct) target values.
    :param y_pred: Estimated target values.
    :param class_mapping: function to map regression values to classes, None if y_pred are classification values
    :param rounding: int or None, None omits rounding, positive integer specifies number of digits
    :return: dict with all used scoring metrics
    """

    try:
        acc = accuracy(y_true, y_pred, class_mapping, rounding)
        p, r, f1 = prec_recall_f1(y_true, y_pred, class_mapping, rounding)
        f1_pn = f1_pn_score(y_true, y_pred, class_mapping, rounding)
        return {'acc': acc, 'prec': p, 'rec': r, 'f1': f1, 'f1_pn': f1_pn}
    except ValueError as err:  # thrown by assert_all_finite
        Log.e(TAG, "'{}'\tReturn None".format(err))
        return None


def confusion_matrix(y_true, y_pred, class_mapping=None):
    """
    Compute confusion matrix using an optional class mapping.

    :param y_true: Ground truth (correct) target values.
    :param y_pred: Estimated target values.
    :param class_mapping: function to map regression values to classes, None if y_pred are classification values
    :return: Confusion matrix
    """
    if class_mapping is not None:
        y_pred = [class_mapping(x) for x in y_pred]

    return metrics.confusion_matrix(y_true, y_pred, labels=LABELS)


class Mapping:
    @staticmethod
    def make_multiclass(y, mapping):
        return [mapping(x) for x in y]

    @staticmethod
    def three_point_map(sentiment):
        """
        Map an arbitrary sentiment [-1, 1], calculated by a classifier,
        to our three point sentiment classification schema (-1, 0, 1).

        :param sentiment: float: value between -1 and 1
        :return: three-point sentiment (-1, 0 or 1)
        """
        if sentiment < -0.4:
            return -1
        elif sentiment > 0.4:
            return 1
        else:
            return 0
