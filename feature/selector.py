import six
from sklearn.base import BaseEstimator, TransformerMixin


class DictUnion(BaseEstimator, TransformerMixin):
    """
    Combine output of transformer which return dictionaries.

    Transforms feature dictionaries from different transformers into one feature dictionary.
    """

    def __init__(self, dict_list):
        self.dict_list = dict_list

    def fit(self, x, y=None):
        return self

    def transform(self, X):
        dicts = []

        # do transform for every transformer
        for name, transformer in self.dict_list:
            if hasattr(transformer, 'active') and not transformer.active:
                continue

            XX = transformer.transform(X)
            XX = [{name + '_' + feat_name: value for feat_name, value in r.items()} for r in XX]  # renaming
            dicts.append(XX)

        # turn output of every transformer (rows of features) into tuple of features per row
        tuples = list(zip(*dicts))

        # merge every dict of tuple into new dict with all features
        ret = []
        for tuple in tuples:
            new_dict = {}
            for dict in tuple:
                new_dict.update(dict)
            ret.append(new_dict)

        return ret

    def _set_params(self, param, **params):
        # Ensure strict ordering of parameter setting:
        # 1. All steps
        if param in params:
            setattr(self, param, params.pop(param))
        # 2. Step replacement
        step_names, _ = zip(*getattr(self, param))
        for name in list(params):
            if '__' not in name and name in step_names:
                self._replace_step(param, name, params.pop(name))
        # 3. Step parameters and other initilisation arguments
        super().set_params(**params)
        return self

    def _get_params(self, param, deep=True):
        out = super().get_params(deep=False)
        if not deep:
            return out
        steps = getattr(self, param)
        out.update(steps)
        for name, estimator in steps:
            if estimator is None:
                continue
            for key, value in six.iteritems(estimator.get_params(deep=True)):
                out['%s__%s' % (name, key)] = value
        return out

    def set_params(self, **params):
        self._set_params('dict_list', **params)
        return self

    def get_params(self, deep=True):
        return self._get_params('dict_list', deep=deep)


class ItemSelector(BaseEstimator, TransformerMixin):
    """For data grouped by feature, select subset of data at a provided key.

    Taken from scikit example
    http://scikit-learn.org/stable/auto_examples/hetero_feature_union.html


    The data is expected to be stored in a 2D data structure, where the first
    index is over features and the second is over samples.  i.e.

    >> len(data[key]) == n_samples

    Please note that this is the opposite convention to sklearn feature
    matrixes (where the first index corresponds to sample).

    ItemSelector only requires that the collection implement getitem
    (data[key]).  Examples include: a dict of lists, 2D numpy array, Pandas
    DataFrame, numpy record array, etc.

    >>> data = {'text':  ["Hello World",
    >>>                   "Example tweet"]
    >>>          'pos':   [[('Hello', '!', 0.9755), ('World', '^', 0.4212)],
    >>>                    [('Example', 'N', 0.9797), ('tweet', 'V', 0.6659)]]}
    >>> ds = ItemSelector(key='text')
    >>> data['text'] == ds.transform(data)

    ItemSelector is not designed to handle data grouped by sample.  (e.g. a
    list of dicts).  If your data is structured this way, consider a
    transformer along the lines of `sklearn.feature_extraction.DictVectorizer`.

    Parameters
    ----------
    key : hashable, required
        The key corresponding to the desired value in a mappable.
    """

    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]


class VectorizerSelector(BaseEstimator):
    """
    Allow changing text vectorizer by parameter.

    Vectorizer can be specified as a parameter and therefore changed in grid searching.
    """

    def __init__(self, vect):
        self.vect = vect

    def fit(self, x, y=None):
        return self.vect.fit(x, y)

    def transform(self, X):
        return self.vect.transform(X)

    def fit_transform(self, X, y=None):
        return self.vect.fit_transform(X, y)
