import csv
from abc import abstractmethod, ABCMeta

from util.log import Log

TAG = __name__


class Lexicon(metaclass=ABCMeta):
    """
    Base class for Sentiment lexicons.
    """

    @abstractmethod
    def __init__(self):
        self.dict = {}  # dict with terms and corresponding score
        self.x_min = None
        self.x_max = None

    @abstractmethod
    def __call__(self, word, **kwargs):
        """
        Lookup the given word in the dictionary.

        :param word: word to lookup sentiment
        :param kwargs: optional parameters
        """
        pass

    def _scale(self):
        """
        Rescaling sentiment scores in range [0, 1] using feature scaling/MinMax scaling.
        """
        self.x_min = min(self.dict.values())
        self.x_max = max(self.dict.values())
        diff = self.x_max - self.x_min
        for k, v in self.dict.items():
            self.dict[k] = (v - self.x_min) / diff

    def scale_dict(self, value_dict):
        """
        Return same dictionary with scaled values adjusted to MinMax of the main dict.

        Main dictionary has to be scaled before calling this method!

        :param value_dict: dict to scale
        :return: copy of the dictionary with scaled values
        """
        self._check_init()
        return {k: self.scale(v) for k, v in value_dict.items()}

    def scale(self, value):
        """
        Return scaled value adjusted to MinMax of the main dict.

        Main dictionary has to be scaled before calling this method!

        :param value: value to scale
        :return: scaled value
        """
        self._check_init()
        diff = self.x_max - self.x_min
        return (value - self.x_min) / diff

    def _check_init(self):
        """
        Raise RuntimeError if dictionary is not scaled.
        """
        if self.x_min is None or self.x_min is None:
            raise RuntimeError("Can't scale value/dict. Lexicon hasn't called _scale yet.")


class Sentiment140Lexicon(Lexicon):
    """
    Sentiment lexicon based on the Sentiment140 Sentiment Lexicon by Saif M. Mohammad et al. [1]

    Contains real value scores for the sentiment of 62,468 terms used in the Sentiment140 corpus
    with sentiment score between -5 (negative) and 5 (positive). Terms include Twitter-specific tokens like
    hashtags and mentions.

    .. seealso::
    NRC-Canada: Building the State-of-the-Art in Sentiment Analysis of Tweets,
    Saif M. Mohammad, Svetlana Kiritchenko, and Xiaodan Zhu,
    In Proceedings of the seventh international workshop on Semantic Evaluation Exercises (SemEval-2013),
    June 2013, Atlanta, USA.
    http://saifmohammad.com/WebPages/lexicons.html
    """

    def __init__(self, path):
        """Initialize lexicon with words and scores from the given path."""
        super().__init__()
        self.path = path

    def read(self):
        with open(self.path, 'r') as f:
            for line in f:
                term, score, n_pos, n_neg = line.split('\t')
                self.dict[term] = float(score)

        self._scale()

    def __call__(self, word, **kwargs):
        return self.dict.get(word)


class MaxDiffLexicon(Lexicon):
    """
    Sentiment lexicon based on the manually annotated MaxDiff Twitter Sentiment Lexicon by Saif M. Mohammad et al. [1]

    Contains real value scores for the sentiment of 1515 terms frequently used in twitter with scores
    between 0 (most negative) and 1 (most positive). Terms include Twitter-specific tokens like
    hashtags, emoticons, misspelled words, negations or abbreviations.

    .. seealso::
    Kiritchenko, S., Zhu, X., Mohammad, S. (2014). Sentiment Analysis of Short Informal Texts.
    Journal of Artificial Intelligence Research, 50:723-762, 2014.
    http://saifmohammad.com/WebPages/lexicons.html

    """

    def __init__(self, path):
        """Initialize lexicon with words and scores from the given path."""
        super().__init__()
        self.path = path

    def read(self):
        with open(self.path, 'r') as f:
            for line in f:
                k, v = line.split('\t')
                self.dict[k] = float(v)

                # no scale, scores are already [0, 1]

    def __call__(self, word, **kwargs):
        return self.dict.get(word)


class AfinnLexicon(Lexicon):
    """
    Sentiment lexicon based on the AFINN lexicon by Finn Årup Nielsen [1].

    The lexicon contains 2477 manually labeled english words (including 16 phrases) with a
    sentiment score between -5 (negative) and 5 (positive).

    After reading the dictionary from the text file the scores will be scaled to the range [0, 1]
    with 0 (negative) and 1 (positive) using feature scaling.

    .. seealso::
    A new ANEW: Evaluation of a word list for sentiment analysis in microblogs,
    Proceedings of the ESWC2011 Workshop on 'Making Sense of Microposts':
    Big things come in small packages 718 in CEUR Workshop Proceedings : 93-98. 2011 May.
    http://arxiv.org/abs/1103.2903
    """

    def __init__(self, path):
        super().__init__()
        self.path = path

    def read(self):
        with open(self.path, 'r') as f:
            for line in f:
                k, v = line.split('\t')
                self.dict[k] = int(v)
        self._scale()  # scale from [-5, 5] to [0, 1]

    def __call__(self, word, **kwargs):
        return self.dict.get(word)


class OpinionLexicon(Lexicon):
    """
    Sentiment lexicon based on the Opinion Lexicon by Bing Liu [1].

    Consists of 2006 positive and 4783 negative words with a score of 1 (positive words) and -1 (negative words).

    After reading the dictionary from the text file the scores will be scaled to the range [0, 1]
    with 0 (negative) and 1 (positive) using feature scaling.

    .. seealso::
    https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html#lexicon

    """

    def __init__(self, path_neg, path_pos):
        super().__init__()
        self.path_neg = path_neg
        self.path_pos = path_pos

    def read(self):
        self._read_words(self.path_pos, 1)
        self._read_words(self.path_neg, -1)
        self._scale()

    def _read_words(self, path, sentiment):
        with open(path, 'r') as f:
            for line in f:
                line = line.strip()
                if line.startswith(';') or line is '':
                    continue
                self.dict[line] = sentiment

    def __call__(self, word, **kwargs):
        return self.dict.get(word)


class SentiStrengthLexicon(Lexicon):
    """
    Sentiment lexicon based on the data set of SentiStrength by Mike Thelwall [1].

    Consists of two different lexicons. One original 2011 corpus (2546 terms) and an improved 2015 corpus (2657 term).
    Each term has a either a positive score [1,2,3,4,5] or a negative score [-1,-2,-3,-4,-5]. The corpus uses word
    truncation to support word trunks. E.g. joy* matches any word starting with joy.

    After reading the dictionary from the text file the scores will be scaled to the range [0, 1]
    with 0 (negative) and 1 (positive) using feature scaling.

    .. seealso::
    http://sentistrength.wlv.ac.uk/

    """

    def __init__(self, path):
        super().__init__()
        self.path = path
        self.trunk_dict = {}
        self.trunk_list = tuple()

    def read(self):
        """
        Read sentiment table from file and return self.
        :return: self
        """
        with open(self.path, 'r') as f:
            trunks = {}
            for line in f:
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
                values = line.split('\t')  # [term, score, comment (optional)]
                term = values[0]
                score = int(values[1])

                # build own dict containing only word trunks
                if term[-1] == '*':
                    trunks[term[:-1]] = score
                    continue

                old = self.dict.get(term)
                if old:
                    # found duplicate (or word w and w/o '*')
                    print("{}: {} == {}".format(term, old, score))
                self.dict[term] = score

        self._scale()  # scale from [-5, 5] to [0, 1]
        self.trunk_dict = self.scale_dict(trunks)  # scale trunk dictionary with the same scales as dict
        self.trunk_list = tuple(self.trunk_dict.keys())  # string.startswith expects tuple
        return self

    def __call__(self, word, **kwargs):

        score = self.dict.get(word)

        # first check in dict to avoid looking for trunk whereas word is in dict (with possible different score)
        if score:
            return score

        # not in dictionary, maybe a trunk
        else:
            # check if word has a trunk at all
            has_trunk = word.startswith(self.trunk_list)

            if has_trunk:
                # check again whole list to find matching trunk (TODO: Better solution with only one loop?)
                for term in self.trunk_list:
                    if word.startswith(term):
                        return self.trunk_dict[term]


class SentiWordNet(Lexicon):
    """
    Sentiment lexicon based on SentiWordNet.

    Calculation of the dictionary is based on the java demo code example from the SentiWordNet homepage:
    http://sentiwordnet.isti.cnr.it/code/SentiWordNetDemoCode.java

    Using this method, the dictionary contains scores for 155287 different words.

    Lexicon can be used with pos tags to restrict lookup at part of speech tags.
    E.g. blue#a and blue#n have different scores, depending on their pos tag.


    :param use_pos: whether to use pos tags for lookup

    .. seealso::
    http://sentiwordnet.isti.cnr.it/
    """

    def __init__(self, path, use_pos):
        super().__init__()
        self.path = path
        self.use_pos = use_pos

    def read(self):
        """
        Build the lookup dictionary from the synsets.

        Code is based on the java demo code from the SentiWordNet page:
        http://sentiwordnet.isti.cnr.it/code/SentiWordNetDemoCode.java

        """
        with open(self.path, 'r') as file:
            reader = csv.reader(file, delimiter='\t')

            tmp_dict = {}

            for i, row in enumerate(reader):

                # skip comments
                if row[0].startswith('#'):
                    continue

                # parse row
                try:
                    pos_tag, id_, pos_score, neg_score, synset, gloss = row[0], int(row[1]), float(row[2]), float(
                        row[3]), \
                                                                        row[4], row[5]
                except ValueError as err:
                    Log.e(TAG, "Error parsing row {}: {}".format(i, err))
                    continue

                # calc score between -1.0 (most negative) and 1.0 (most positive)
                score = pos_score - neg_score

                # add all terms of current synset
                for synterm in synset.split(' '):
                    term, rank = synterm.split('#')
                    rank = int(rank)
                    if self.use_pos:
                        term = term + '#' + pos_tag

                    # add term if not in dict or else retrieve term
                    data = tmp_dict.setdefault(term, {rank: score})
                    # add new meaning to term
                    data.setdefault(rank, score)

            # build dictionary by calculating one single score
            for term, synset_scores in tmp_dict.items():

                # Calculate weighted average. Weigh the synsets according to their rank.
                # Score = 1 / 2 * first + 1 / 3 * second + 1 / 4 * third.....etc.
                # Sum = 1 / 1 + 1 / 2 + 1 / 3...
                score = 0.0
                sum_ = 0.0
                for rank, sc in synset_scores.items():
                    score += sc / rank
                    sum_ += 1.0 / rank
                score /= sum_
                self.dict.setdefault(term, score)
        self._scale()

    def extract(self, word, tag):
        if self.use_pos:
            word += '#' + tag
        return self.dict.get(word)

    def __call__(self, word, **kwargs):
        return self.extract(word, kwargs.get('tag'))


if __name__ == '__main__':
    swn = SentiWordNet("../data/sentiment_lexicons/SentiWordNet.txt", use_pos=True)
    swn.read()

    opinion = OpinionLexicon("../data/sentiment_lexicons/opinion_lexicon_liu/negative_words.txt",
                             "../data/sentiment_lexicons/opinion_lexicon_liu/positive_words.txt")
    opinion.read()

    afinn_lexicon = AfinnLexicon("../data/sentiment_lexicons/AFINN-111.txt")
    afinn_lexicon.read()

    maxDiff = MaxDiffLexicon("../data/sentiment_lexicons/Maxdiff-Twitter-Lexicon_0to1.txt")
    maxDiff.read()

    sent140 = Sentiment140Lexicon("../data/sentiment_lexicons/Sentiment140_unigrams-pmilexicon.txt")
    sent140.read()

    strength = SentiStrengthLexicon("../data/sentiment_lexicons/sentistrength/SentiStrength_2015.txt")
    strength.read()

    print("\nSentiWordNet")
    print("good#a {}".format(swn.extract("good", "a")))
    print("bad#a {}".format(swn.extract("bad", "a")))
    print("blue#a {}".format(swn.extract("blue", "a")))
    print("blue#n {}".format(swn.extract("blue", "n")))

    print("\nOpinion Lexicon")
    print("good {}".format(opinion("good")))
    print("bad {}".format(opinion("bad")))

    print("\nAFINN")
    print("good {}".format(afinn_lexicon("good")))
    print("bad {}".format(afinn_lexicon("bad")))

    print("\nmaxDiff")
    print("good {}".format(maxDiff("good")))
    print("bad {}".format(maxDiff("bad")))

    print("\nSentiment140")
    print("good {}".format(sent140("good")))
    print("bad {}".format(sent140("bad")))

    print("\nSentiStrength")
    print("good {}".format(strength("good")))
    print("bad {}".format(strength("bad")))
    print("optimism {}".format(strength("optimism")))  # optimism    2 (0.7)
    print("optimal {}".format(strength("optimimal")))  # optimi*     1 (0.6)
