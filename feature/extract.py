import re
from collections import Counter

import numpy as np
from nltk.sentiment import SentimentIntensityAnalyzer
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted

from data.emoticons import NEUT_EMOTICONS, POS_EMOTICONS, NEG_EMOTICONS
from pipeline import ActivatableComponent
from tokenize_ import tokenizer
from util.log import Log


class PunctuationExtractor(BaseEstimator, TransformerMixin):
    """
    Count the number of contiguous sequences of !, ? and a mix of both and if the last token contains at least a ! or ?.
    """
    exl_pattern = re.compile('!+')
    quest_pattern = re.compile('\?+')
    mix_pattern = re.compile('(!|\?)+')

    last_pattern = re.compile('.*(!|\?).*')

    def fit(self, x, y=None):
        return self

    def transform(self, pos_tags):

        features = []
        for tags in pos_tags:
            freq = {'punct_!': 0, 'punct_?': 0, 'punct_both': 0}  # number of !, ? and both
            last_token = tags[-1][0]  # whether last token is ! or ?
            for token, tag, _ in tags:
                if PunctuationExtractor.mix_pattern.match(token):
                    freq['punct_both'] += 1
                elif PunctuationExtractor.quest_pattern.match(token):
                    freq['punct_?'] += 1
                elif PunctuationExtractor.exl_pattern.match(token):
                    freq['punct_!'] += 1

            if PunctuationExtractor.exl_pattern.match(last_token) or PunctuationExtractor.quest_pattern.match(
                    last_token):
                freq['punct_last'] = True
            else:
                freq['punct_last'] = False

            features.append(freq)

        return features


class POSCounter(BaseEstimator, TransformerMixin):
    """
    Count the different types of POS-tags in a given list of tags.
    """

    def __init__(self, normalize):
        """
        Create a POSCounter to count POS tags.

        :param normalize: whether to normalize the score by dividing each one by the number of tags (# of tokens).
        """
        super().__init__()
        self.normalize = normalize

    def fit(self, x, y=None):
        return self

    def transform(self, pos_tags):

        features = []
        for tags in pos_tags:
            freq = Counter()
            for token, tag, _ in tags:

                if tag in ['N', 'O', '^', 'S', 'Z']:  # nouns
                    freq.update('N')
                elif tag in ['V']:  # verb
                    freq.update('V')
                elif tag in ['A']:  # adjective
                    freq.update('A')
                elif tag in ['R']:  # adverb
                    freq.update('R')
                elif tag in ['!']:  # interjections (e.g. 'yeah', 'hi', 'well')
                    freq.update('!')
                elif tag in ['#', '@', '~', 'U']:  # twitter/online specific
                    freq.update('#')
                elif tag in ['E']:  # emoticon
                    freq.update('E')

            if self.normalize:
                for tag in freq.keys():
                    freq[tag] /= len(tags)

            features.append(freq)

        return features


class SentimentExtractor(ActivatableComponent):
    """
    Calculate a sentiment score using a given sentiment lexicon.
    """

    def __init__(self, lexicon, strip_hash=True):
        """
        Create a SentimentExtractor with a given lexicon.

        :param lexicon:
            callable to lookup a given word with a given pos tag
        :param strip_hash:
            whether to remove #-symbol or not
        """
        super().__init__()
        self.lexicon = lexicon
        self.strip_hashtag = strip_hash

    def build_lexicon(self):
        """
        Build the lexicon which the extractor will use.

        :return: the lexicon to use
        """
        return self.lexicon

    def fit(self, x, y=None):
        return self

    def transform(self, pos_tags):

        lexicon_extractor = self.build_lexicon()

        features = []
        for tags in pos_tags:
            scores = []  # score of each token
            for token, tag, confidence in tags:

                # always skip mention (usernames have no sentiment)
                if tag == '@':
                    continue

                # remove # from hashtags
                if tag == '#' and self.strip_hashtag:
                    token = token[1:]

                score = lexicon_extractor(token)
                if score is not None:
                    scores.append(score)

            # take sum of sentiment scores as a single feature for each tweet
            sum_ = sum(scores)
            num_pos = sum(score > 0.55 for score in scores)
            num_neg = sum(score < 0.45 for score in scores)
            num = len(scores)
            num_neut = num - num_pos - num_neg

            feats = {'sum': sum_, 'num_pos': num_pos, 'num_neut': num_neut, 'num_neg': num_neg, 'num': num}

            max_pos = 0
            max_neg = 0

            if num_pos:
                max_pos = max(scores)
            if num_neg:
                max_neg = min(scores)

            feats.update({'max_pos': max_pos,
                          'max_neg': max_neg})

            features.append(feats)

        return features


class VaderSentimentExtractor(BaseEstimator, TransformerMixin):
    """
    Calculate sentiment of a tweet using VADER [1].

    VADER uses rule-based analysis and sentiment lexicons to compute the sentiment score of a tweet.
    Each tweet gets 4 scores: positive, neutral, negative and compound

    .. seealso::
    Hutto, C.J. & Gilbert, E.E. (2014)
    VADER: A Parsimonious Rule-based Model for Sentiment Analysis of Social Media Text.
    Eighth International Conference on Weblogs and Social Media (ICWSM-14). Ann Arbor, MI, June 2014.
    https://github.com/cjhutto/vaderSentiment
    """

    TAG = __name__

    def fit(self, X=None, y=None):
        self.vader_ = SentimentIntensityAnalyzer()
        return self

    def transform(self, X):
        check_is_fitted(self, ['vader_'])
        features = []
        for tweet in X:
            sent = self.vader_.polarity_scores(tweet)
            sent['compound'] = (sent['compound'] + 1) / 2  # scale compound score to [0, 1]
            features.append(sent)

        return features


class EmoticonExtractor(BaseEstimator, TransformerMixin):
    """
    Extract the emoticons of a tweet and categorize them into positive, negative or neutral.
        """
    TAG = __name__

    def __init__(self, show_error=False):
        super().__init__()
        self.show_error = show_error
        self.pos = list(map(str.lower, POS_EMOTICONS))
        self.neut = list(map(str.lower, NEUT_EMOTICONS))
        self.neg = list(map(str.lower, NEG_EMOTICONS))

    def fit(self, x, y=None):
        return self

    def transform(self, pos_tags):

        features = []
        for tags in pos_tags:
            freq = Counter({'emot_pos': 0, 'emot_neg': 0, 'emot_neut': 0})
            for token, tag, confidence in tags:
                # check if the token can be treated as a Emoticon
                if tag == 'E' and confidence > 0.7:
                    if token in self.pos:
                        freq.update(['emot_pos'])
                    elif token in self.neg:
                        freq.update(['emot_neg'])
                    elif token in self.neut:
                        freq.update(['emot_neut'])
                    else:
                        if self.show_error:
                            Log.e(self.TAG,
                                  "Unknown Emoticon '{}',  POS-TAG: {}, in '{}'".format(token, (token, tag, confidence),
                                                                                        " ".join(
                                                                                            [token for token, tag, _ in
                                                                                             tags])))
            features.append(freq)
        return features


class POSExtractor(BaseEstimator, TransformerMixin):
    """Extract the POS Tags from a list of tweets.

    Current representations:

    - plain text
    - pos tags

    Take a sequence of tweets and produces a dict of sequences. Keys are
    `text` and `pos`.
    Please note that this is the opposite convention to sklearn feature
    matrices (where the first index corresponds to sample).

    Parameters
    ----------
    pos_tagger : callable or None (default)
        Override the CmuPosTokenizer POS Tagger step.

    Example
    -------
    >>> data = {'text':  ["Hello World",
    >>>                   "Example tweet"]
    >>>          'pos':   [[('Hello', '!', 0.9755), ('World', '^', 0.4212)],
    >>>                    [('Example', 'N', 0.9797), ('tweet', 'V', 0.6659)]]}
    >>> ds = ItemSelector(key='text')
    >>> data['text'] == ds.transform(data)

    Note
    ----
    Make use of Numpy's Record array (numpy.recarray) so the data representation
    doesn't look exactly like the example data above.

    """
    TAG = __name__

    def __init__(self, pos_tagger=None):
        super().__init__()
        self.pos_tagger = pos_tagger

    def build_tagger(self):
        """Return a function that extracts POS tags from a list of documents"""
        if self.pos_tagger is not None:
            return self.pos_tagger

        return tokenizer.CmuPosTokenizer()

    def fit(self, x, y=None):
        return self

    def transform(self, tweets):

        # create empty Record array which can be accessed dict-like
        features = np.recarray(shape=(len(tweets),),
                               dtype=[('text', object), ('pos', object)])

        extract_pos_tags = self.build_tagger()

        pos_tags = extract_pos_tags(tweets)

        for i, tags in enumerate(pos_tags):
            features['text'][i] = tweets[i]
            features['pos'][i] = tags

        return features
