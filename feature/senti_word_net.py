import csv

from util.log import Log

TAG = __name__


class SentiWordNet:
    def __init__(self, path, use_pos):
        self.path = path
        self.dict = {}  # dict with terms and corresponding score
        self.use_pos = use_pos

    def read(self):
        with open(self.path, 'r') as file:
            reader = csv.reader(file, delimiter='\t')

            tmp_dict = {}

            for i, row in enumerate(reader):

                # skip comments
                if row[0].startswith('#'):
                    continue

                # parse row
                try:
                    pos_tag, id_, pos_score, neg_score, synset, gloss = row[0], int(row[1]), float(row[2]), float(row[3]), \
                                                                    row[4], row[5]
                except ValueError as err:
                    Log.e(TAG, "Error parsing row {}: {}".format(i, err))
                    continue

                # calc score between -1.0 (most negative) and 1.0 (most positive)
                score = pos_score - neg_score

                # add all terms of current synset
                for synterm in synset.split(' '):
                    term, rank = synterm.split('#')
                    rank = int(rank)
                    if self.use_pos:
                        term = term + '#' + pos_tag

                    # add term if not in dict or else retrieve term
                    data = tmp_dict.setdefault(term, {rank: score})
                    # add new meaning to term
                    data.setdefault(rank, score)

            # build dictionary by calculating one single score
            for term, synset_scores in tmp_dict.items():

                # Calculate weighted average. Weigh the synsets according to their rank.
                # Score = 1 / 2 * first + 1 / 3 * second + 1 / 4 * third.....etc.
                # Sum = 1 / 1 + 1 / 2 + 1 / 3...
                score = 0.0
                sum_ = 0.0
                for rank, sc in synset_scores.items():
                    score += sc / rank
                    sum_ += 1.0 / rank
                score /= sum_
                self.dict.setdefault(term, score)

    def extract(self, word, tag):
        if self.use_pos:
            word += '#' + tag
        return self.dict.get(word)

    def __call__(self, word, tag):
        return self.extract(word, tag)


if __name__ == '__main__':
    swn = SentiWordNet("../data/SentiWordNet.txt", use_pos=True)
    swn.read()
    print("good#a {}".format(swn.extract("good", "a")))
    print("bad#a {}".format(swn.extract("bad", "a")))
    print("blue#a {}".format(swn.extract("blue", "a")))
    print("blue#n {}".format(swn.extract("blue", "n")))
