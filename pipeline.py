from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.pipeline import Pipeline, FeatureUnion


class ActivatablePipeline(Pipeline):
    """
    Custom Pipeline which can be deactivated by setting the 'active' parameter (e.g. in gridsearch)
    """
    def __init__(self, steps, active=True):
        super().__init__(steps)
        self.active = active

    def fit(self, X, y=None, **fit_params):
        if self.active:
            super().fit(X, y, **fit_params)

    def _transform(self, X):
        if self.active:
            return super()._transform(X)
        else:
            return X


class ActivatableFeatureUnion(FeatureUnion):
    """
    Custom FeatureUnion where parts of the union can be deactivated by setting the 'active' parameter (e.g. in
    gridsearch)
    """
    def _iter(self):
        lst = list(super()._iter())
        lst = [(name, trans, weight)
               for name, trans, weight in lst
               if not hasattr(trans, 'active') or trans.active]
        return lst

    def _update_transformer_list(self, transformers):
        # don't update, as we don't know which transformer has been deactivated
        pass


class ActivatableComponent(BaseEstimator, TransformerMixin):
    """
    Custom estimator/transformer which can be deactivated by setting the 'active' parameter.
    """
    def __init__(self, active=True):
        super().__init__()
        self.active = active

    def fit_transform(self, X, y=None, **fit_params):
        if self.active:
            return super().fit_transform(X, y, **fit_params)
        else:
            return X

    def set_params(self, **params):
        return self._set_params('active', **params)

    def _set_params(self, param, **params):
        if param in params:
            setattr(self, param, params.pop(param))
        return super().set_params(**params)

    def get_params(self, deep=True):
        return self._get_params('active', deep)

    def _get_params(self, param, deep=True):
        out = super().get_params(deep=False)
        if not deep:
            return out
        params = getattr(self, param)
        out.update([(param, params)])
        return out
