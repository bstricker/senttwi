from enum import Enum

import numpy as np
import yaml
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from data.tweet_reader import TweetReader
from feature.extract import POSCounter, EmoticonExtractor, SentimentExtractor, VaderSentimentExtractor, \
    POSExtractor, PunctuationExtractor
from feature.lexicon import SentiWordNet, OpinionLexicon, AfinnLexicon, MaxDiffLexicon, Sentiment140Lexicon, \
    SentiStrengthLexicon
from feature.selector import ItemSelector, VectorizerSelector, DictUnion
from fm.pywFMWrapper import FMRegression
from pipeline import ActivatablePipeline, ActivatableFeatureUnion
from tokenize_.postprocessor import PrunePostProcessor, StemmingPostProcessor, StopwordPostProcessor, \
    LowercasePostProcessor, PosPostProcessor, RepeatingPostProcessor
from tokenize_.preprocessor import SubstitutePreprocessor
from tokenize_.tokenizer import CmuPosTokenizer, TwokenTokenizer
from util.log import Log

# supported classifiers
ClassifierType = Enum('ClfType', ['FM', 'Ada', 'Bagging', 'SVM'])
GridSearchMode = Enum('GridSearchMode', ['ALL', 'FEATURE', 'CLF'])

TAG = __name__


class Config(object):
    """Config class used to read yaml config file."""

    def __init__(self, path):
        """
        Load yaml configuration.
        :param path: yaml file
        """
        self.path = path
        self.cfg = None
        with open(path, 'r') as yamlfile:
            self.cfg = yaml.load(yamlfile)

    def get_tweets(self, verbose=False):
        """
        Return tweets and labels for train and test set.

        Settings in config.yaml:

        - n_train: size of the training set (null uses all)
        - n_test: size of the test set (null uses all)
        - use_dev: use additional dev train file
        - undersample: Use undersampling (cut number of tweets for every class to the smallest class)
        - oversample: Use oversampling (copy tweets until each class has the same size)
        - shuffle: shuffle data before splitting into train and test set
        - paths for train-, test- and train_dev_file


        :rtype (list, np.ndarray, list, np.ndarray)
        :return: train_tweets, train_labels, test_tweets, test_labels
        """
        paths = self.cfg['paths']
        n_train = self.cfg['n_train']
        n_test = self.cfg['n_test']
        use_dev = self.cfg['use_dev_file']

        shuffle = self.cfg['shuffle']

        train_reader = TweetReader(path=paths['three_train_file'], n=n_train, verbose=verbose)
        tweets, y_train = train_reader.read_tweets()

        if use_dev:
            train_dev_reader = TweetReader(path=paths['three_train_dev_file'], n=n_train, verbose=verbose)
            tweets_d, y_train_d = train_dev_reader.read_tweets()

            tweets.extend(tweets_d)
            y_train = np.concatenate((y_train, y_train_d))

        test_reader = TweetReader(path=paths['three_test_file'], n=n_test, verbose=verbose)
        tweets_test, y_test = test_reader.read_tweets()

        # shuffle train and test set and build new sets with same number of tweets
        if shuffle:
            tweets.extend(tweets_test)
            y_train = np.concatenate((y_train, y_test))
            tweets, tweets_test, y_train, y_test = train_test_split(
                tweets, y_train, test_size=len(tweets_test), random_state=42)

        if n_train:
            tweets, y_train = tweets[:n_train], y_train[:n_train]

        if n_test:
            tweets_test, y_test = tweets_test[:n_test], y_test[:n_test]

        return tweets, y_train, tweets_test, y_test

    def get_sampler(self):
        undersample = self.cfg['undersample']
        oversample = self.cfg['oversample']

        if undersample and oversample:
            Log.e(TAG, "No sampling method for over- and undersampling found which supports multiclass")
            return None
        if undersample:
            return RandomUnderSampler()
        if oversample:
            return RandomOverSampler()

    def get_clf_name(self):
        return self.cfg['clf']['use']

    def get_clf_type(self):
        """
        Get name from config and return ClassifierType enum

        :return: enum from ClassifierType
        """
        clf_name = self.get_clf_name()
        if clf_name == ClassifierType.FM.name:
            return ClassifierType.FM
        elif clf_name == ClassifierType.Ada.name:
            return ClassifierType.Ada
        elif clf_name == ClassifierType.SVM.name:
            return ClassifierType.SVM
        elif clf_name == ClassifierType.Bagging.name:
            return ClassifierType.Bagging
        else:
            Log.e(TAG, "Invalid Classifier Type in [clf][use]")

    def get(self, *keys):

        """
        Get value from config file.

        To get a nested value, use multiple keys.

        When keys reference a leaf node, a value is returned. When referencing a intermediate key,
        either a dict or a list, depending on the used YAML construct, is returned.

        :param keys: one ore more keys
        :return: single value or dict/list if non-leaf node
        """
        item = self.cfg

        for i, key in enumerate(keys):
            try:
                item = item[key]
            except KeyError:
                nested = list(keys[:i])

                # use virtual root node in error msg
                if not len(nested):
                    nested = ['root']
                Log.e(TAG,
                      "KeyError: {} has no key '{}' defined in config file '{}'".format(nested, key, self.path))

                return None
        return item

    def get_sent_extractors(self):
        """
        Return list of tuples containing the lexicon name and the lexicon object.

        Read lexicons section and create SentimentExtractor objects containing each enabled lexicon.
        Returned list is a tuple (lexicon_name, sent_extractor) and can be used with DictUnion.

        Due the nature of dicts, there is no order in the extractor list.

        :return: list of tuples with name and sentiment extractor.

        """

        extractors = []

        for name, params in self.cfg['lexicons'].items():

            if not params['enabled']:
                continue

            if name == 'swn':
                lex = SentiWordNet(params['path'], use_pos=params['use_pos'])
                lex.read()
                extractor = SentimentExtractor(lex)  # no # in swn lexicon
            elif name == 'opinion':
                lex = OpinionLexicon(params['path_neg'],
                                     params['path_pos'])
                lex.read()
                extractor = SentimentExtractor(lex)  # no # in opinion lexicon
            elif name == 'afinn':
                lex = AfinnLexicon(params['path'])
                lex.read()
                extractor = SentimentExtractor(lex)  # no # in AFINN lexicon
            elif name == 'maxDiff':
                lex = MaxDiffLexicon(params['path'])
                lex.read()
                extractor = SentimentExtractor(lex, strip_hash=False)  # MaxDiff contains hashtags
            elif name == 'sent140':
                lex = Sentiment140Lexicon(params['path'])
                lex.read()
                # Sent14 contains hashtags (and also @ but these are ignored because of the automated dataset)
                extractor = SentimentExtractor(lex, strip_hash=False)
            elif name == 'sStrength':
                lex = SentiStrengthLexicon(params['path'])
                lex.read()
                extractor = SentimentExtractor(lex)  # no # in SentiStrength
            else:
                Log.d('config', 'ignore {}'.format(name))
                continue

            extractors.append((name, extractor))

        return extractors

    def build_clf(self):
        """
        Return classifier set in config file.

        Exit program if an invalid classifier is used.
        Currently only FM and Ada are supported.

        :return: classifier
        """
        clf = self.get_clf_type()
        params = self.cfg['clf'][self.get_clf_name()]

        if clf == ClassifierType.FM:
            Log.d(TAG, "Using pywFM as classifier")
            if params:
                return FMRegression(**params)
            else:
                return FMRegression()

        elif clf == ClassifierType.Ada:
            Log.d(TAG, "Using AdaBoost as classifier")
            if params:
                dtc_params = {k.split('__')[1]: v for k, v in params.items() if k.split('__')[0] == "decision_tree"}
                params = {k: v for k, v in params.items() if '__' not in k}
                dtc = DecisionTreeClassifier(**dtc_params)
                return AdaBoostClassifier(base_estimator=dtc, **params)
            else:
                return AdaBoostClassifier()

        elif clf == ClassifierType.Bagging:
            Log.d(TAG, "Using Bagging as classifier")
            if params:
                dtc_params = {k.split('__')[1]: v for k, v in params.items() if k.split('__')[0] == "decision_tree"}
                params = {k: v for k, v in params.items() if '__' not in k}
                dtc = DecisionTreeClassifier(**dtc_params)
                return BaggingClassifier(base_estimator=dtc, **params)
            else:
                return BaggingClassifier()

        elif clf == ClassifierType.SVM:
            Log.d(TAG, "Using SVM as classifier")
            if params:
                return SVC(**params)
            else:
                return SVC()
        else:
            Log.e(TAG, "Invalid ClassifierType {}. Exiting...".format(clf))
            exit()

    def build_pipeline(self):
        """Build the full classification pipeline specified in the config file."""
        return self._build_pipeline()

    def _build_pipeline(self):
        # use name from config
        clf_name = self.cfg['clf']['use']
        return Pipeline([

            ('pre', self.build_preprocessor()),

            # extract pos tags and add them to tweets
            ('tagger', self.build_tagger()),

            # combine features into a union
            ('union', self.build_feature_union()),

            # chi2 feature selection
            ('selection', self.build_feature_selector()),

            # classifier
            (clf_name, self.build_clf())

        ])

    def build_tagger(self):
        """
        Return POS Tagger used to calculate POS tags specified in config file.

        Build PostProcessor to use in CmuPosTokenizer if postprocess has been set in config file.

        :return: preprocessor
        """
        post = self.cfg['pipeline']['preprocess']['postprocess']

        if post:
            post = self.build_postprocessor('post_pos')

        tokenizer = CmuPosTokenizer(post)
        return POSExtractor(pos_tagger=tokenizer)

    def is_feature_enabled(self, name):
        try:
            return self.cfg['pipeline']['feature'][name]['enabled']
        except KeyError as error:
            Log.e(TAG, 'Keyerror: No {} in pipeline setup in config file'.format(error))
            return None

    def build_feature_union(self):
        """
        Return feature union used in pipeline to calculate the features.
        :return: feature union containing transformer to calculate features
        """
        union = []
        for feature, params in self.cfg['pipeline']['feature'].items():

            if not params['enabled']:
                continue

            if feature == 'text_vect':
                step = ActivatablePipeline([
                    ('select', ItemSelector(key='text')),
                    ('vect', self.build_vectorizer())
                ])
            elif feature == 'pos':
                step = ActivatablePipeline([
                    ('select', ItemSelector(key='pos')),
                    ('counter', POSCounter(params['normalize'])),
                    ('dict', DictVectorizer())
                ])
            elif feature == 'emoticon':
                step = ActivatablePipeline([
                    ('select', ItemSelector(key='pos')),
                    ('extract', EmoticonExtractor()),
                    ('dict', DictVectorizer())
                ])
            elif feature == 'lexicons':
                step = ActivatablePipeline([
                    ('select', ItemSelector(key='pos')),
                    ('union', DictUnion(self.get_sent_extractors())),
                    ('dict', DictVectorizer())
                ])
            elif feature == 'vader':
                step = ActivatablePipeline([
                    ('select', ItemSelector(key='text')),
                    ('extract', VaderSentimentExtractor()),
                    ('dict', DictVectorizer())
                ])
            elif feature == 'punct':
                step = ActivatablePipeline([
                    ('select', ItemSelector(key='pos')),
                    ('extract', PunctuationExtractor()),
                    ('dict', DictVectorizer())
                ])
            else:
                Log.d('config', 'ignore pipeline step {}'.format(feature))
                continue

            union.append((feature, step))
        return ActivatableFeatureUnion(union)

    def build_postprocessor(self, name):
        """
        Return postprocessor with the given name specified in config file.
        :param name: name of the post processor in yaml file
        :return: postprocessor
        """
        postprocessors = []
        for name, params in self.cfg[name].items():

            if not params['enabled']:
                continue

            cpy = {k: v for k, v in params.items() if k != 'enabled'}

            if name == 'stem':
                post = StemmingPostProcessor()
            elif name == 'stop':
                post = StopwordPostProcessor()
            elif name == 'lower':
                post = LowercasePostProcessor()
            elif name == 'repeat':
                post = RepeatingPostProcessor()
            elif name == 'pos':
                post = PosPostProcessor(**cpy)
            elif name == 'prune':
                post = PrunePostProcessor(**cpy)
            else:
                Log.d(TAG, 'ignore postprocessor {}'.format(name))
                continue

            postprocessors.append(post)

        # return single postprocessor
        if len(postprocessors) == 1:
            return postprocessors[0]

        return postprocessors

    def build_vectorizer(self):
        """
        Return text vectorizer specified in config file.

        Used Tokenizer can be either the default tokenizer or own TwokenTokenizer.
        If TwokenTokenizer is used, allows to use PostProcessor if postprocess has been set in config file.

        Supported Vectorizer:

            - TfIdf:    TfIdfVectorizer
            - Count:    CountVectorizer

        Setting parameters in vectorizer (beside tokenizer) are currently not supported.

        :return: vectorizer
        """

        vect_name = self.cfg['pipeline']['feature']['text_vect']['use']

        tok_name = self.cfg['vect']['tokenizer']
        tok = None
        postprocessor = None

        params = self.cfg['vect'][vect_name]

        if tok_name['enabled']:  # use TwokenTokenizer
            if tok_name['postprocess']:  # use postprocessor in TwokenTokenizer
                postprocessor = self.build_postprocessor('post_text')
            tok = TwokenTokenizer(postprocessor)

        if vect_name == 'TfIdf':
            vect = TfidfVectorizer(tokenizer=tok, **params)
        elif vect_name == 'Count':
            vect = CountVectorizer(tokenizer=tok, **params)
        else:
            Log.d('config', 'unknown vectorizer {}'.format(vect_name))
            exit()

        return VectorizerSelector(vect)

    def build_preprocessor(self):
        """Return a configured SubstitutePreprocessor"""
        paths = self.cfg['paths']
        pre = self.cfg['preprocessor']
        negation = paths['map_negation'] if pre['negation'] else None
        nominal = paths['map_nominal'] if pre['nominal'] else None
        slang = paths['map_slang'] if pre['slang'] else None

        return SubstitutePreprocessor(active=pre['active'], lower=pre['lower'],
                                      negation=negation, nominal_verb=nominal, slang=slang)

    def build_feature_selector(self):
        """Return a configured Chi2 Feature Selection component"""
        selection = self.cfg['pipeline']['selection']
        return SelectKBest(chi2, k=selection['k'])


if __name__ == '__main__':
    cfg = Config('config.yaml')

    # get single value
    value = cfg.get('n_train')

    # get whole dict item
    value = cfg.get('clf')

    # get nested item
    value = cfg.get('gridsearch', 'use_pos_cache')

    # get value in pipeline
    value = cfg.get("pipeline", 'feature', 'text_vect', 'use')

    # invalid keys return None
    value = cfg.get('invalid')
    assert value is None

    # invalid nested keys return also None
    value = cfg.get('gridsearch', 'invalid')
    assert value is None
