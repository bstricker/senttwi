import os
import sys

from util import util

""" Convert a json result file from a grid search to a csv file. """

if __name__ == '__main__':

    input_file = output_file = None

    if len(sys.argv) >= 2:
        input_file = sys.argv[1]
        if len(sys.argv) == 3:
            output_file = sys.argv[2]

    else:
        print('Usage {} input_file [output_file]'.format(sys.argv[0]))
        exit()

    if not os.path.isfile(input_file):
        print('File does not exits: {}'.format(input_file))
        exit()

    if not output_file:
        output_file = input_file + '.csv'

    print("Converting '{}' to '{}'".format(input_file, output_file))
    util.json_to_csv(input_file, output_file)
