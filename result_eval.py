import csv
import json
import sys
import os.path
from datetime import datetime
from pathlib import Path


def avg(x: list) -> float:
    """
    Calculate average of a list. Non lists are converted to lists.
    :param x: list
    :return: average of the list
    """
    if type(x) is not list: x = [x]
    return sum(x) / len(x)


def find_files(folder):
    """
    Return list of json files in the given folder and all subfolders. 
    :param folder: initial folder to find files
    :return: list of 'results_cv.json' files
    """
    path = Path(folder)
    return list(path.glob('**/results_cv.json'))


if __name__ == '__main__':

    start_folder = None

    if len(sys.argv) == 2:
        start_folder = sys.argv[1]
    else:
        print('Usage result_eval.py start_folder')
        exit()

    if not os.path.isdir(start_folder):
        print('Start folder does not exits: {}'.format(start_folder))
        exit()

    files = find_files(start_folder)

    # write output into new folder
    best_folder = Path(start_folder) / 'best' / str(datetime.utcnow())
    best_folder.mkdir(parents=True)

    for file in files:

        results = []
        params = []
        fields = []

        # parse result file
        print('read {}'.format(file))
        with file.open() as json_file:
            for obj in json_file:
                param, res = json.loads(obj)
                params.append(param)
                unpacked = {k: v for k, v in res.items() if not isinstance(v, list)}
                # unpack grouped class results (e.g. {f1: [x, x, x]} into {f1_-1: x, f1_0: x, f1_1: x})
                for k, v in res.items():
                    if isinstance(v, list):
                        unpacked.update({"{}_{}".format(k, i): vv for i, vv in enumerate(v, start=-1)})
                results.append((param, unpacked))
                fields = unpacked.keys()

        # map results from folder structure to correct grid search
        run = file.parts[-2]
        clf = file.parts[-3]
        gs = file.parts[-4]

        fields = sorted(fields)
        metrics = fields.copy()

        # gs, clf, run, metric1, ..., metricN, params
        fields.insert(0, 'run')
        fields.insert(0, 'clf')
        fields.insert(0, 'gs')
        fields.append('params')

        # calc best result for each metric and write in overall file
        for metric in metrics:
            metric_file = best_folder / (metric + '.csv')
            writeHeader = True

            if metric_file.exists():
                writeHeader = False

            with metric_file.open(mode='a') as csvfile:
                csvWriter = csv.DictWriter(csvfile, fieldnames=fields)
                if writeHeader:
                    csvWriter.writeheader()
                # first result with max value of curr metric
                # but other results with same max value could have better values for other metrics
                first_best = max(results, key=lambda x: avg(x[1][metric]))[1]

                # all results with best [metric]
                bests = [(k, v) for k, v in results if v[metric] == first_best[metric]]

                # result with the highest [metric] and highest overall metrics (require that all metrics should be
                # maximized)
                all_best = max(bests, key=lambda x: sum(m for m in x[1].values()))
                row = {}
                row.update(all_best[1])
                row.update({'run': run, 'clf': clf, 'gs': gs, 'params': sorted(all_best[0].items())})
                csvWriter.writerow(row)
