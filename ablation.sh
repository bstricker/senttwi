#!/bin/bash

clf=$1
abl=$2
prog="main.py"

for run in {1..3}
do
	(
	echo "Run $run"

	echo "Running $prog (Run $run)"
	python3 -u "$prog" > "${prog%.*}_$run.txt" 2> "error_$run.txt"

	echo "Copying results (Run $run)"
	rsync -a config.* $prog ${prog%.*}_$run.txt error_$run.txt 2017_results/final/$1/ablation/$2/$run
	) & # spawn each run in subprocess...
done

wait # and wait for all to finish

echo "Done"

# beep 3x
echo -e '\a' > /dev/tty
sleep 1
echo -e '\a' > /dev/tty
sleep 1
echo -e '\a' > /dev/tty
