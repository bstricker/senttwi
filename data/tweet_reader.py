import csv
import time
from collections import Counter

from numpy import asarray

from util.log import Log
from util.time import to_readable_time


class TweetReader:
    TAG = __name__

    def __init__(self, path, n=None, dtype=float, five_point=False, verbose=False):
        """Read tweets from a given tab-separated file (.tsv) and return list of tweets and labels/sentiment.

        Expect same tsv format used in the SemEval 2016 Task 4 files.
        Lines starting with # are considered as comments and are ignored.

        :param path: path of the tsv file
        :param n: int, default=None
                optional number of tweets to read, None if all tweets should be read,
        :param dtype: type of the sentiment
        :param five_point: boolean, default=False
                True if 5-class sentiments are use, else False
        :param verbose: boolean, default=False
                print summary after reading tweets
        """
        self.path = path
        self.n = n
        self.dtype = dtype
        self.five_point = five_point
        self.verbose = verbose

        self.freq = Counter()  # frequency of each class
        self.weights = {}

    def read_tweets(self):
        if self.verbose:
            Log.d(self.TAG, "Reading tweets from '{}'".format(self.path))
        t = time.time()
        tweets = []
        y = []
        self.freq = Counter()
        with open(self.path, 'r') as file:
            reader = csv.reader(filter(lambda row : row[0] != '#', file), delimiter='\t', quoting=csv.QUOTE_NONE)
            i = 0
            skipped = 0
            ids = []

            try:
                for t_id, sent, tweet in reader:

                    # exit if number is specified
                    if self.n is not None and i == self.n:
                        break

                    sentiment = self._get_sentiment(sent)

                    # skip invalid sentiments
                    if sentiment is None or tweet == 'Not Available' or t_id in ids:
                        skipped += 1
                        continue

                    ids.append(t_id)
                    tweets.append(tweet)
                    y.append(self.dtype(sentiment))

                    i += 1
            except ValueError as e:
                print('At tweet No {}. {}'.format(i + skipped + 1, e))
                raise e

            # calculate frequency and class weights
            n = len(tweets)
            self.freq.update(y)
            max_cls = max(self.freq)
            self.weights = {cls: self.freq[max_cls] / freq for cls, freq in self.freq.items()}

            if self.verbose:
                Log.d(self.TAG,
                      "Finish reading {} tweets after '{}'".format(n, to_readable_time(time.time() - t)))
                Log.d(self.TAG, "{} skipped tweets".format(skipped))
                Log.d(self.TAG, "Frequency:\t{}".format(dict(self.freq)))
                Log.d(self.TAG, "Weights:\t{}".format(self.weights))

            return tweets, asarray(y)

    def _get_sentiment(self, sent):
        if not self.five_point:
            # convert textual sentiment into numerical (-1, 0, 1)
            if sent == 'negative':
                return -1
            elif sent == 'positive':
                return 1
            elif sent == 'neutral':
                return 0
            else:
                Log.e(self.TAG,
                      "Unexpected three-point sentiment {}. Only 'negative', 'positive' or 'neutral' allowed".format(
                          sent))
        else:
            # sentiment is already in correct format, check for correct range (-2, -1, 0, 1, 2)
            if sent in range(-2, 2):
                return sent
            else:
                Log.e(self.TAG,
                      "Unexpected five-point sentiment {}. Sentiment has to be in the range[-2, 2]".format(
                          sent))
