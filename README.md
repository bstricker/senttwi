# SentTwi

SentTwi is an approach to classify Tweets by their sentiment using Factorization Machines and Ensemble Methods. SentTwi is part of my Master Thesis on "Sentiment Detection of Tweets using Factorization Machines and Ensemble Methods". After reading the Tweets from the `.csv` files in `data`, a Feature Pipeline is used to calculate the feature vector. The pipelien consists of a SubstitutePreprocessor, a tokenizer and a Union of various features (Bag of Words, POS-Tags, Emoticons, Sentiment lexicons, ...).

## Installation

SentTwi uses various components to work correctly. Therefore it depends on some libraries.

*Installation works on Ubuntu (should work on other distros too)*

### Dependencies
#### Programming Language

* **Python3**	SentTwi is written in Python3
* **C** 		xxHash lib
* **C++**		libFM
* **Java**	POS Tagger and Vader


#### Install SentTwi

Clone SentTwi and all submodules
	
	git clone --recursive https://bstricker@bitbucket.org/bstricker/senttwi.git

#### Install Dependencies

Setup Requirements

	pip3 install -r ./requirements.txt

Install pyyaml
	Download package from [pyyaml.org](http://pyyaml.org/wiki/PyYAML) and follow the installation instructions to install **PyYAML** and **LibYAML**

Setup pywFM
	
	cd /home/senttwi/libs/libfm
	make all
	export LIBFM_PATH=/home/senttwi/libs/libfm/bin/

## Usage

SentTwi consists of three main scripts:

* `main.py`: Runs a single classifier
* `main_cv.py`: Starts a KFold Cross Validation for a single classifier
* `main_grid_search.py`: Starts an exhaustive grid search for one type of classifier

All configuration is done in `config.yaml`.  
Tweets and lexicon-data is stored in the `data` folder.

### Available classifiers

**Factorization machine**  
Uses [pywFM](https://github.com/bstricker/pywFM) wrapper around C++ [libFM](https://github.com/bstricker/libfm) library.
Both libraries are forked due to some minor changes necessary to work better with scikit.

**AdaBoost**
Uses scikit-learn's [AdaBoost](http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html) Classifier

**Bagging**
Uses scikit-learn's [Bagging](http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.BaggingClassifier.html) Classifier

**SVM**
Uses scikit-learn's [SVM](http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html) Classifier

## History

TODO: Write history

## License

TODO: Write license