import json
import pickle
from multiprocessing import Lock

import numpy as np
from sklearn import clone
from sklearn.externals.joblib import Parallel
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import StratifiedKFold
from sklearn.pipeline import Pipeline

from evaluate import evaluate
from fm.pywFMWrapper import BaseFM
from model_selection.cv import KFoldCV
from util.encode import OwnJSONEncoder
from util.log import Log
from util.parallel import effective_jobs

TAG = __name__
STEP_SIZE = 5


def _build_cv_name(results_file):
    split = results_file.rsplit('.', 1)

    if len(split) > 1:
        return split[0] + '_cv.' + split[-1]

    else:
        return split[0] + '_cv.json'


class GridSearch(object):
    """
    Run a grid search over a search space to find the best parameters for an estimator.

    Grid search can be run multi-threaded to decrease runtime of the search.

    In contrast to sklearn's GridSearchCV class, this grid search can also handle
    estimators pywFM and pyFM classifier but doesn't use cross validation.
    """
    lock = Lock()  # global lock to ensure correct writing to json file by the thread pool

    def __init__(self, base_estimator, param_grid, jobs=1, n_folds=1, sampler=None, results_file=None, overwrite=False,
                 verbose=True):
        """
        :param base_estimator:  An estimator to instantiate for every grid point.
                                Has to implement the sklearn estimator interface.
        :param param_grid:  Dictionary with parameters names (string) as keys
                            and lists of parameter settings to try as values.

        :param jobs:    Number of jobs to run in parallel. Default is 1.
                        If -1 all CPUs are used. If 1 is given, no parallel computing code
                        is used at all, which is useful for debugging. For jobs below -1,
                        (n_cpus + 1 + n_jobs) are used. Thus for jobs = -2, all
                        CPUs but one are used.
        :param n_folds:   Number of folds, every number <= 1 will use no cross validation
        :param results_file:   write the results as json objects into this file during the search
        :param overwrite:   if an existing save_json file should be overwritten
        :param verbose:     turn verbosity on (additional messages during search)
        """
        self.base_estimator = base_estimator  # estimator to clone for every thread
        self.param_grid = param_grid
        self.jobs = effective_jobs(jobs)
        self.n_folds = 1 if n_folds < 1 else n_folds
        self.sampler = sampler
        self.results_file = results_file
        self.cv_file = _build_cv_name(results_file)
        self.overwrite = overwrite
        self.verbose = verbose
        self.results = []

        self.use_cv = True if n_folds > 1 else False

    def run(self, X, y, X_test=None, y_test=None):
        """
        Run the grid search with the given data.

        If no CV is used, it's possible to use X_test and y_test as the training set, else 25 % of the training data
        will be held out and used as test data.

        :param X: training set data
        :param y: training labels
        :param X_test:  optional test set data
        :param y_test:  optional test labels
        """
        # open file in write mode to delete content
        if self.overwrite and self.results_file:
            open(self.results_file, 'w').close()
            if self.use_cv:
                open(self.cv_file, 'w').close()

        data = self._compute_data(X, y, X_test, y_test)

        self._grid_search(data)

    def _compute_data(self, X, y, X_test, y_test):
        """
        Compute and return the data (train and test sets) for different cross validation settings.

        - If cross validation (n_folds set) is used, then a StratifiedKFold is used.
        - If test set (X_test and y_test) is not None, no cv is used and the given train-test split is returned.
        - If no cv should be used but also no test set is given, split the data 75 % train and 25 % test.

        :param X: training set data
        :param y: training labels
        :param X_test:  optional test set data
        :param y_test:  optional test labels
        :return: tuple containing splitted sets (X_train, X_test, y_train, y_test)
        :rtype: (list, list, np.ndarray, np.ndarray)
        """
        if self.use_cv and (X_test is not None or y_test is not None):
            Log.e(TAG, "Ignore test set parameters for cross validation")
            X_test = None
            y_test = None

        if self.use_cv:
            # stratified n-Fold
            kfold = StratifiedKFold(n_splits=self.n_folds)
            cv = kfold.split(X, y)
        elif X_test is not None and y_test is not None:
            # return train-test sets unchanged
            return [(X, X_test, y, y_test)]
        else:
            # single split with 75 % train and 25 % test data
            kfold = StratifiedKFold(n_splits=1 / 0.25)
            cv = list(kfold.split(X, y))[:1]

        # build list of 4-tuple (X_train, X_test, y_train, y_test)
        # if cv, each entry is one split, if no cv is used, only one tuple is returned
        data = [([X[i] for i in train_indices], [X[i] for i in test_indices],
                 y[train_indices], y[test_indices])
                for train_indices, test_indices in cv]

        if self.sampler:
            data_resampled = []
            for x_train, x_test, y_train, y_test in data:
                x_train_resampled, y_train_resampled = KFoldCV.resample(self.sampler, x_train, y_train)
                KFoldCV.check_split(x_train_resampled, x_test)
                data_resampled.append((x_train_resampled, x_test, y_train_resampled, y_train))
        return data

    def _grid_search(self, data):

        # use sklearn's ParameterGrid to iterate over search space
        search = ParameterGrid(self.param_grid)
        self.length = len(search)
        if self.verbose:
            Log.d(TAG, 'GridSearch with {} different configurations and {} jobs'.format(self.length, self.jobs))
            if self.use_cv:
                Log.d(TAG, 'Using {}-fold for CV'.format(self.n_folds))

        parallel = Parallel(n_jobs=self.jobs, verbose=self.verbose)

        results = parallel(
            delayed(self._execute)(clone(self.base_estimator), i, param, X_train, X_test, y_train, y_test)
            for i, param in enumerate(search)
            for X_train, X_test, y_train, y_test in data)

        # safety check for any valid result
        try:
            valid_score = next((score[1] for score in results if score[1]))
        except StopIteration:
            Log.e(TAG, "No valid results.")
            # self.results = None
            return

        # average fold results
        if self.use_cv:
            scores = valid_score
            avg_results = []
            for param in search:
                # get all results for a given parameter setting
                cv_results = list(filter(lambda x: x[0] == param, results))
                avgs = {}
                for score in scores:
                    # calc mean of all results
                    mean = np.mean([row[1][score] for row in cv_results if row[1]], axis=0).tolist()
                    avgs[score] = mean
                avg_results.append((param, avgs))
            if self.results_file:
                GridSearch.lock.acquire()
                try:
                    with open(self.cv_file, 'a') as file_name:
                        for result in avg_results:
                            json.dump(result, file_name, sort_keys=True, cls=OwnJSONEncoder)
                            print('', file=file_name)  # append newline after json line
                finally:
                    GridSearch.lock.release()

            results = avg_results

        self.results = results

    def _execute(self, estimator, i, param, x_train, x_test, y_train, y_test):

        # sort params to ensure that nested params are set in the right order (shorter name first)
        # e.g. 'union__text__vect__vect' has to be set before 'union__text__vect__vect__tokenizer'
        for k, v in sorted(param.items()):
            try:
                estimator.set_params(**{k: v})
            except AttributeError:
                # catch error when trying to set parameter on an non existent object
                Log.e(TAG,
                      "ERROR: Try to set parameter '{}'. Maybe '{}' is None. Ignoring...".format(k, k.split('__')[-2]))
                param[k] = 'IGNORED_NONE' + str(v)

            except ValueError:
                # catch error when trying to set invalid parameter
                Log.e(TAG, "ERROR: Invalid parameter '{}'. Ignoring...".format(k))
                keys = estimator.get_params().keys()
                Log.e(TAG, "Valid parameter are: {}".format(sorted(keys)))
                param[k] = 'IGNORED_INVALID' + str(v)

        # use own wrapper for sklearn interfaces
        if isinstance(estimator, BaseFM):
            estimator.fit(x_train, y_train, y_test)  # 3rd parameter with test labels needed in predict
            pred = estimator.predict(x_test)

        # estimators with sklearns fit and predict methods
        else:
            fit_params = {}
            # add test labels
            if isinstance(estimator, Pipeline) and isinstance(estimator.steps[-1][-1], BaseFM):
                name = estimator.steps[-1][0]
                fit_params = {name + '__y_test': y_test}

            try:
                estimator.fit(x_train, y_train, **fit_params)
                pred = estimator.predict(x_test)
            except ValueError as error:
                Log.d(TAG, "'{}'\tIgnoring result".format(error))
                pred = None

        if pred is not None:
            result = evaluate.eval_all(y_test, pred, evaluate.Mapping.three_point_map)
        else:
            result = None

        res = (param, result)

        if self.results_file:
            GridSearch.lock.acquire()
            try:
                with open(self.results_file, 'a') as file_name:
                    json.dump(res, file_name, sort_keys=True, cls=OwnJSONEncoder)
                    print('', file=file_name)  # append newline after json line
            finally:
                GridSearch.lock.release()

        if self.verbose:
            Log.d(TAG, "Param {} finish".format(i))

        return res

    def best_acc(self):
        """
        Return the test result with the best accuracy score.
        :return: best result
        """
        if not self.results:
            return None
        return max(self.results, key=lambda x: x[1]['acc'])

    def best_f1(self):
        """
        Return the test result with the best mean f1 score.
        :return: best result
        """
        if not self.results:
            return None
        return max(self.results, key=lambda x: np.mean(x[1]['f1']))

    def best_f1_pn(self):
        """
        Return the test result with the best F1 p/n (mean of F1 pos and F1 neg) score.

        This score is used to evaluate systems participating at the SemEval Challenge.
        :return: best result
        """
        if not self.results:
            return None
        return max(self.results, key=lambda x: x[1]['f1_pn'])

    def save_results(self, file_name, write_json=True):
        """
        Save result list persistently on disk.
        :param file_name: name of the file
        :param write_json:  bool, default=True
                            True if the list should be saved as a json object, False if binary.
        """
        if write_json:
            with open(file_name, 'w') as file_name:
                json.dump(self.results, file_name, indent='\t')
        else:
            with open(file_name, 'wb') as file_name:
                pickle.dump(self.results, file_name)

    @staticmethod
    def sprint_result(result, sort=False):
        """
        Return a formatted string of the given results.

        :param result: result from grid search, format: (params, scores) tuple
        :param sort: whether both dicts should be sorted
        :return: formatted string
        :rtype str
        """

        if not result:
            return None

        # convert both dictionaries to a list of pairs/tuples
        params = list(result[0].items())
        scores = list(result[1].items())

        if sort:
            params = sorted(params, key=lambda t: t[0])
            scores = sorted(scores, key=lambda t: t[0])

        return "Parameters: {}\tScores: {}".format(params, scores)

    @staticmethod
    def load_results(file_name, read_json=True):
        """
        Load result list from a given file.

        Note: This method only reads the data from the file.
        No manipulating of a GridSearch object is done. Thus
        load_results is a static method.

        :param file_name:   name of the file
        :param read_json:   bool, default=True
                            True if the file contains readable json objects, False if binary.
        :return: result list
        """
        if read_json:
            with open(file_name, mode='r') as file:
                return json.load(file)
        else:
            with open(file_name, mode='rb') as file:
                return pickle.load(file)


# copied from sklearn's external joblib/parallel.py
def delayed(function, check_pickle=True):
    """Decorator used to capture the arguments of a function.

    Pass `check_pickle=False` when:

    - performing a possibly repeated check is too costly and has been done
      already once outside of the call to delayed.

    - when used in conjunction `Parallel(backend='threading')`.

    """
    # Try to pickle the input function, to catch the problems early when
    # using with multiprocessing:
    if check_pickle:
        pickle.dumps(function)

    def delayed_function(*args, **kwargs):
        return function, args, kwargs

    try:
        import functools
        delayed_function = functools.wraps(function)(delayed_function)
    except AttributeError:
        " functools.wraps fails on some callable objects "
    return delayed_function
