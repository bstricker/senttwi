from multiprocessing.pool import Pool

import numpy as np
from sklearn import clone
from sklearn.model_selection import StratifiedKFold
from sklearn.pipeline import Pipeline

from evaluate import evaluate
from fm.pywFMWrapper import FMRegression, BaseFM
from util.log import Log
from util.parallel import effective_jobs

TAG = __name__


class KFoldCV(object):
    """
    Use a Stratified K-Fold cv iterator to cross validate a given classifier.

    Stratified k-folds preserve the percentage of samples of each class.

    KFoldCV can be run multi-threaded to compute multiple validation sets at once.

    In contrast to sklearn's cross_validation classes, this cv can also handle
    pywFM and pyFM classifier.

    """

    def __init__(self, clf, data, y, n_folds=5, jobs=1, sampler=None):
        """
        Create new KFoldCV object.

        :param clf:     Classifier to cross validate
        :param data:    Data to split into train and test set
        :param y:       Labels to split into train and test labels
        :param n_folds: Number of folds. Default is a 5-fold
        :param jobs:    Number of jobs to run in parallel. Default is 1.
                        If -1 all CPUs are used. If 1 is given, no parallel computing code
                        is used at all, which is useful for debugging. For jobs below -1,
                        (n_cpus + 1 + n_jobs) are used. Thus for jobs = -2, all
                        CPUs but one are used.
        """
        super().__init__()
        self.base_estimator = clf
        self.data = np.array(data)
        self.y = y
        self.n_folds = n_folds
        self.jobs = effective_jobs(jobs)
        self.sampler = sampler

    def cross_val_score(self, mapping=None, averaging=False, shuffle=False):
        """
        Evaluate the classifier by cross validation.

        :param mapping:     function to map regression values to classes or
                            None if classifier computes classes. Default None
        :param averaging:   Compute average over all cv results
        :param shuffle:     Whether to shuffle the data before splitting
        :return:            list of results for each run of the cross validation or
                            one result if averaging is used
        """
        Log.d(TAG, "kFold cv with {} folds and {} jobs".format(self.n_folds, self.jobs))

        results = []
        with Pool(processes=self.jobs) as pool:

            kfold = StratifiedKFold(n_splits=self.n_folds, shuffle=shuffle)
            for train_index, test_index in kfold.split(self.data, self.y):
                x_train = self.data[train_index]
                y_train = self.y[train_index]
                x_test = self.data[test_index]
                y_test = self.y[test_index]

                if self.sampler:
                    KFoldCV.resample(self.sampler, x_train, y_train)
                self.check_split(x_train, x_test)

                # add search thread to the pool and use async result to get results later
                res = pool.apply_async(self._execute,
                                       args=(clone(self.base_estimator), x_train, x_test, y_train, y_test, mapping))

                results.append(res)

            pool.close()
            pool.join()

        # evaluate all async results to get quality results
        results = [result.get() for result in results]

        if averaging:
            res = {}
            for key in results[0].keys():
                # use axis 0 to keep classes in recall, precision, ...
                res[key] = np.mean([result[key] for result in results], axis=0)
            return res

        return results

    @staticmethod
    def resample(sampler, x, y):
        # fit_sample needs a 2-dim X vector, so add 2nd dim before and remove after sampling
        x_train_resized = np.expand_dims(x, axis=1)
        x_train_resized, y = sampler.fit_sample(x_train_resized, y)
        x = x_train_resized.squeeze()
        return x, y

    @staticmethod
    def check_split(x_train, x_test):
        intersection = set(x_test).intersection(x_train)

        if intersection:
            Log.e(TAG, "{} samples in train AND test set. Wrong CV split".format(len(intersection)))
            Log.e(TAG, intersection)

    def _execute(self, estimator, x_train, x_test, y_train, y_test, mapping):
        """
        Execute one train-test run on the given estimator. Evaluate the result with the given mapping function.

        :param estimator:   estimator to train and test on
        :param x_train:     train data
        :param x_test:      test data
        :param y_train:     train labels
        :param y_test:      test labels
        :param mapping:     function to map regression values to classes
        :return:            result of the estimator
        """

        # use own wrapper for sklearn interfaces
        if type(estimator) == FMRegression:
            estimator.fit(x_train, y_train, y_test)  # 3rd parameter with test labels needed in predict
            pred = estimator.predict(x_test)

        # estimators with sklearns fit and predict methods
        else:
            fit_params = {}
            # add test labels
            if isinstance(estimator, Pipeline) and isinstance(estimator.steps[-1][-1], BaseFM):
                name = estimator.steps[-1][0]
                fit_params = {name + '__y_test': y_test}

            estimator.fit(x_train, y_train, **fit_params)
            pred = estimator.predict(x_test)

        result = evaluate.eval_all(y_test, pred, mapping, rounding=4)

        return result
