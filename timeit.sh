#!/bin/sh

SETUP="
#  Put your Python initialization code here!
pass
"

EXEC="
#  Put your Python execution code here!
pass
"

EXEC2="
#  Put your Python execution code to compare here!
pass
"

python -m timeit -s "$SETUP" "$EXEC"
python -m timeit -s "$SETUP" "$EXEC2"