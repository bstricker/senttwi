from enum import Enum
from pprint import pprint

import numpy
from sklearn.pipeline import Pipeline

import config
from evaluate import evaluate
from fm.pywFMWrapper import BaseFM
from model_selection.cv import KFoldCV
from util import util
from util.log import Log
from util.time import Stopwatch

TAG = __name__

ClassifierType = Enum('ClfType', ['BaseFM', 'Ada'])

TYPE = ClassifierType.BaseFM
CLF_PIPE_NAME = 'clf'  # name of the classifier in the pipeline


def get_feature_names(pipeline):
    # get parts of pipeline

    feats = []

    for id, pipe_obj in pipeline.named_steps['union'].transformer_list:
        if getattr(pipe_obj, 'active', True):
            pipe_obj = pipe_obj.named_steps
            if id == 'text_vect':
                feats.extend(pipe_obj['vect'].vect.get_feature_names())
            else:
                feats.extend(pipe_obj['dict'].feature_names_)

    selection = pipeline.named_steps.get('selection')
    if selection:
        # get only selected features
        mask = selection.get_support()
        feats = numpy.array(feats)[mask]  # use np array to support masked indexing

    return feats


if __name__ == "__main__":

    config = config.Config('config.yaml')
    sampler = config.get_sampler()

    print_features = config.get('print_features')

    stop = Stopwatch(start=False)

    stop.start('Reading Tweets')
    tweets, y_train, tweets_test, y_test = config.get_tweets(verbose=True)
    stop.stop()

    if sampler:
        tweets, y_train = KFoldCV.resample(sampler, tweets, y_train)

    pipeline = config.build_pipeline()

    # separate clf from pipeline to get feature matrix
    clf = pipeline.steps[-1][-1]
    pipeline = Pipeline(pipeline.steps[:-1])

    stop.restart('Fit Transform Training Pipeline')
    x_train = pipeline.fit_transform(tweets, y_train)
    stop.stop()

    stop.restart('Fit Training Classifier')
    if isinstance(clf, BaseFM):
        clf.fit(x_train, y_train, y_test=y_test)
    else:
        clf.fit(x_train, y_train)
    stop.stop()

    stop.restart('Transform Test Pipeline')
    x_test = pipeline.transform(tweets_test)
    stop.stop()

    stop.restart('Predict Test Classifier')
    preds = clf.predict(x_test)
    stop.stop()

    stop.restart('Print evaluation metrics')
    if print_features:
        feat = get_feature_names(pipeline)
        util.print_feature_names(feat)
        util.print_classifications(tweets_test, x_test, preds, y_test, feat, to_file=True)

    pprint(sorted(evaluate.eval_all(y_test, preds, evaluate.Mapping.three_point_map, rounding=4).items(), key=lambda t: t[0]))
    print(evaluate.confusion_matrix(y_test, preds, evaluate.Mapping.three_point_map))
    stop.stop()
