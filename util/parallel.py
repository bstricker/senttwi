import os


def effective_jobs(jobs):
    if jobs == 0:
        raise ValueError('jobs == 0 in Parallel has no meaning')
    elif jobs is None:
        # multiprocessing is not available or disabled, fallback
        # to sequential mode
        return 1
    elif jobs < 0:
        cpu_count = os.cpu_count()
        # use sequential mode if cpus can't be detected
        if not cpu_count:
            return 1
        jobs = max(cpu_count + 1 + jobs, 1)
    return jobs
