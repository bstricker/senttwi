"""
    Module with helper methods to handle csr_matrices.
    Support reading and writing csr matrices to text files.
    Comparing two csr_matrices and reading a prediction file written by libFM.

    Each row in the saved matrix contains a training case (x, y), where the
    first value is the target value y and the remaining values are the non-zero values of x.

    Support text format of libFM to feed the C++ program with own data.

    :Example:

    >>>    4 0:1.5 3:-7.9
    >>>    2 1:0.5 3:2
    >>>    -1 6:1

    .. seealso::

    - libFM       https://github.com/srendle/libfm
    - csr_matrix  http://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html

"""
from scipy.sparse import csr_matrix


def write_matrix(path, x, y):
    """
    Save the feature vector x and the labels y as a text file.

    :param path: name of the file
    :param x: feature vector
    :param y: labels
    """
    with open(path, 'w') as f:
        for label, feat in zip(y, x):
            s = str(label)
            for ind, val in zip(feat.indices, feat.data):
                s += ' {}:{}'.format(ind, val)
            f.write(s + '\n')


def read_matrix(path):
    """
    Read matrix from file.

    :param path: text file containing the csr matrix to be read
    :return: list of labels y, csr matrix x
    """
    with open(path, 'r') as f:
        y = []
        indptr = [0]
        indices = []
        values = []
        # build csr matrix incrementally
        for row in f:
            data = row.strip().split(sep=' ')
            y.append(float(data.pop(0)))
            for ind, val in [pair.split(':') for pair in data]:
                indices.append(ind)
                values.append(val)
            indptr.append(len(indices))
        return y, csr_matrix((values, indices, indptr), dtype=float)


def read_lib_fm(path):
    """
    Read the prediction output file written by libFM.

    :param path: text file containing the prediction list to be read
    :return: list of predictions
    """
    with open(path, 'r') as f:
        return [float(line) for line in f]


def compare(m1, m2):
    """
    Compare two csr matrices by subtracting them element-wise and checking if no non-zero are present.

    :rtype: bool True if m1 and m2 are equal, else False
    """
    return (m1 - m2).getnnz() == 0
