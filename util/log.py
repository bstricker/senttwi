import sys
import time

__author__ = 'bstricker'


class Log:
    """
    Log class to enable formatted logging and logging to file
    """
    OUTPUT = './log.txt'
    ERROR_OUTPUT = './error_log.txt'
    TIME_FORMAT = "%d.%m.%y %H:%M:%S"
    NAME_WIDTH = 15
    TO_FILE = False

    @staticmethod
    def d(tag, msg, file=OUTPUT, both=False):
        # log to file if global variable is set or a specific file is given
        log_file = Log.TO_FILE or (file is not None and file != Log.OUTPUT)
        if log_file:
            out = open(file, 'a', encoding='utf-8')
        else:
            out = sys.stdout

        string = "{:{width}}\t{}\t{}".format(tag[:Log.NAME_WIDTH], time.strftime(Log.TIME_FORMAT), msg,
                                             width=Log.NAME_WIDTH)

        print(string, file=out)
        if both and log_file:
            print(string, file=sys.stdout)

        if log_file:
            out.close()

    @staticmethod
    def e(tag, msg, file=ERROR_OUTPUT, both=False):
        # log to file if global variable is set or a specific file is given
        log_file = Log.TO_FILE or (file is not None and file != Log.ERROR_OUTPUT)
        if log_file:
            out = open(file, 'a', encoding='utf-8')
        else:
            out = sys.stderr
        string = "{:{width}}\t{}\t{}".format(tag[:Log.NAME_WIDTH], time.strftime(Log.TIME_FORMAT), msg,
                                             width=Log.NAME_WIDTH)
        print(string, file=out)
        if both and log_file:
            print(string, file=sys.stderr)

        if log_file:
            out.close()
