import csv
import json
import smtplib
import sys
import xxhash

# same as string.punctuation except # and @
from email.mime.text import MIMEText

from evaluate.evaluate import LABELS

punctuation = """!"$%&'()*+,-./:;<=>?[\]^_`{|}~"""


def compute_hash(docs):
    """
    Compute xxh64 hash of the list of tweets.
    :param docs: ndarray of tweets
    :return: digest in byte


    .. seealso:: xxhash lib: https://github.com/ifduyue/python-xxhash

    """
    xxh = xxhash.xxh64()
    for tweet in docs:
        xxh.update(tweet)
    doc_hash = xxh.digest()
    return doc_hash


def json_to_csv(json_filename, csv_filename):
    """
    Convert a given JSON results file to a csv file.

    Can only convert a JSON file containing the results written by the grid search with the following format:
    Each row contains a JSON array consisting of two dicts.

    :param json_filename: name of the json file to read from
    :param csv_filename: name of the csv file to save
    """
    results = []
    fieldnames = []
    with open(json_filename, 'r') as json_file:
        with open(csv_filename, 'w') as csv_file:
            w = csv.DictWriter(csv_file, fieldnames)
            for obj in json_file:
                params, res = json.loads(obj)

                unpacked = {k: v for k, v in res.items() if not isinstance(v, list)}
                # unpack grouped class results (e.g. {f1: [x, x, x]} into {f1_-1: x, f1_0: x, f1_1: x})
                for k, v in res.items():
                    if isinstance(v, list):
                        if len(v) != 3:
                            raise AssertionError("No three-point scoring result: {}".format(v))
                        unpacked.update({"{}_{}".format(k, sent_label): vv for sent_label, vv in zip(LABELS, v)})


                # get names in first loop
                if not fieldnames:
                    fieldnames.extend(sorted(params.keys()))
                    fieldnames.extend(sorted(unpacked.keys()))
                    field_names = set(fieldnames)
                    w.fieldnames = fieldnames
                    w.writeheader()

                # merge both dicts to get one dict to write
                params.update(unpacked)
                results.append(params)

                # check for differences in field names and parameter names
                param_nams = set(params.keys())
                if param_nams != field_names:
                    extra_params = param_nams - field_names
                    missing_params = field_names - param_nams
                    print("Inconsistent parameters: Diffs: Extra {} vs Missing {} for {}".format(
                        extra_params, missing_params, params))
                    print("Removing extraneous params and writing NaN for missing params.")

                    for extra in extra_params:
                        del params[extra]

                    for missing in missing_params:
                        params[missing] = float('nan')

                w.writerow(params)


def send_mail(subject, body, from_mail, to_mail, smtp_host, smtp_port, login_user, login_pw):
    """
    Send email.
    :param subject: subject of the email 
    :param body:  simple text body of the email
    :param from_mail: mail address of the sender
    :param to_mail: mail address of the receiver
    :param smtp_host: address of the smtp server to use
    :param smtp_port: port number to connect to smtp server
    :param login_user: user name to login to smtp server
    :param login_pw: password to login to smtp server
    """
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_mail
    msg['To'] = to_mail

    # Send the message via our own SMTP server.
    try:
        with smtplib.SMTP(smtp_host, smtp_port) as s:
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(login_user, login_pw)
            s.send_message(msg)
            s.close()
        print("Email sent")
    except Exception as ex:
        print("Unable to send the email. Error: ", ex, file=sys.stderr)


def print_feature_names(feature_names, row_length=10):
    """
    Print the given feature names with indices in a formatted way.

    :param feature_names: list of feature names
    :param row_length: num of feature names per printed row
    """
    feature_names = list(enumerate(feature_names))

    for i in range(0, len(feature_names), row_length):
        print(feature_names[i:i + row_length])


def print_classifications(docs, X, y_pred, y_true, features, single_precision=True, to_file=False):
    """
    Print information about a multiclass classification with [-1, 0, 1] as class labels.
     
    Assembles input and output data to print an extensive report about the classified documents.
     
    Parameters
    ----------
    docs : list
        Raw documents which have been classified
    X : sparse matrix of shape = [n_samples, n_features]
        The testing input samples.
    y_pred : list of length n_samples
        The predicted target values (class labels).
    y_true : list of length n_samples
        The ground truth target values (class labels).
    features : list of length n_features
        The feature names
    single_precision : bool
        If `True`, print only one decimal place
    to_file : bool
        If `True`, information will be printed to three files
        (grouped by precision classification error) instead to `stdout`.
    """
    if len(docs) != X.shape[0] or len(features) != X.shape[1]:
        raise ValueError("X hasn't correct shape")
    if len(y_pred) != len(y_true):
        raise ValueError("Predictions and ground truth shape mismatch")

    if to_file:
        fp = open('false_positive.txt', mode='w')
        fnt = open('false_neutrals.txt', mode='w')
        fn = open('false_negative.txt', mode='w')

    use_mapping = True if set(y_pred) != set(y_true) else False

    # print calculated features for each test document
    for i, doc in enumerate(docs):
        nonzero = X[i].nonzero()[1]
        if single_precision:
            vals = ['{:.1f}'.format(X[i, ny]) for ny in nonzero]
        else:
            vals = [X[i, ny] for ny in nonzero]
        feats = [features[nz] for nz in nonzero]

        from evaluate import evaluate
        mapped_pred = evaluate.Mapping.three_point_map(y_pred[i]) if use_mapping else int(y_pred[i])

        if mapped_pred != y_true[i]:
            f = sys.stdout
            if to_file:
                if mapped_pred == 1:
                    f = fp
                if mapped_pred == 0:
                    f = fnt
                if mapped_pred == -1:
                    f = fn

            print('{pred}{mapped: }\t{correct[0]}\t'
                  '{true: }\t{tweet}\t{feats}'.format(pred='{: .1f}\t'.format(y_pred[i]) if use_mapping else '',
                                                      mapped=mapped_pred, correct=str(mapped_pred == y_true[i]),
                                                      true=int(y_true[i]), tweet=doc, feats=list(zip(feats, vals))),
                  file=f)
