import time

from util.log import Log


class Stopwatch:
    """
    Stop the time and print stopped time (optional).
    """
    TAG = __name__

    def __init__(self, tag=None, log=None, start=True, msg=None):
        """
        Create new Stopwatch.

        :param tag: Name of the stopwatch
                    optional, if None, 'Stopwatch' is used
        :param log: bool, default=None
                    True if start/stop messages should be printed
                    If None, default value from config file will be used.
        :param start: bool, default=True
                    True if the clock should be started after initializing
        :param msg: string, optional, message to display if log is enabled

        """
        self._start = 0
        self._stop = 0
        self.tag = tag if tag is not None else self.TAG
        if log is None:
            from config import Config
            log = Config('config.yaml').get('log_stopwatch')
        self._log = log
        if start:
            self.start(msg)

    def start(self, msg=None):
        """
        Start the Stopwatch.

        Call is only valid, if watch hasn't been started yet. Else use restart instead.

        :param msg: string, optional, message to display if log is enabled
        :return: Stopwatch object for method chaining
        """
        if self._start != 0:
            Log.e(self.tag, "Already started, ignoring...")
            return self
        if self._log:
            message = "Start"
            if msg:
                message = message + '\t' + msg
            Log.d(self.tag, message)
        self._start = time.time()
        return self

    def stop(self, msg=None):
        """
        Stop the Stopwatch.

        If log has been set, print elapsed time in a readable format.

        :param msg: string, optional, message to display if log is enabled
        :return: time in seconds since watch has been started
        """
        self._stop = to_readable_time(time.time() - self._start)
        if self._log:
            message = "Elapsed time: {}".format(self._stop)
            if msg:
                message = message + '\t' + msg
            Log.d(self.TAG, message)
        return self._stop

    def reset(self):
        """
        Reset the Stopwatch.

        :return: Stopwatch object
        """
        self._start = 0
        return self

    def restart(self, msg=None):
        """
        Restart the Stopwatch.

        Should be called, if the clock has been used before.

        :param msg: string, optional, message to display if log is enabled
        :return: Stopwatch object
        """
        return self.reset().start(msg)


def to_readable_time(seconds):
    """
    Return a readable presentation of amount of seconds
    :param seconds: time in seconds
    :return: readable string of amount of seconds
    """
    times = [('s', 60), ('min', 60), ('h', 24), ('d', -1)]

    # special more precise case
    if seconds < 5:
        return '{:.2f} s'.format(seconds)

    time_str = ''
    time = int(seconds)

    for suffix, factor in times:

        if time > factor != -1:
            mod = divmod(time, factor)
            time_str = "{} {} {}".format(mod[1], suffix, time_str)
            time = mod[0]
        else:
            return "{} {} {}".format(time, suffix, time_str)
