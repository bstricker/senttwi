from json import JSONEncoder

import numpy
from sklearn.base import BaseEstimator

from tokenize_.postprocessor import BasePostProcessor
from tokenize_.tokenizer import BaseTokenizer


class OwnJSONEncoder(JSONEncoder):
    """
    Customized JSONEncoder to dump complex classes.

    NOTE: Encoder dumps only string representation. So loading objects of such dumps will not work!

    Support of the following classes:

    - Estimators (based on sklearn's BaseEstimator)
    - Tokenizers
    - PostProcessors
    - numpy int and float dtypes (int32, int64, float64, ...)
    """

    def default(self, o):

        if isinstance(o, BaseEstimator):
            o2 = o.__class__()
            params = o.get_params(False)
            def_params = o2.get_params(False)
            diff = {k: v for k, v in params.items() if k in def_params and v != def_params[k]}
            if diff:
                # use tuple of name and non default params
                return o.__class__.__name__, diff
            else:
                # use class name if no parameter has been changed
                return o.__class__.__name__

        elif isinstance(o, BaseTokenizer):
            if o.postprocessor:
                return o.__class__.__name__, o.postprocessor
            else:
                return o.__class__.__name__

        elif isinstance(o, BasePostProcessor):
            return o.__class__.__name__

        elif numpy.dtype(o) in [int, float]:
            # use string representation
            return str(o)

        else:
            print('type ({}) = {}'.format(str(o), type(o)))
            return super().default(o)
