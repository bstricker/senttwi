class MissingArgumentError(Exception):
    """
    Raised when an optional argument missing.

    Mostly needed if interfaces define a parameter as optional but the implementation needs this parameter.
    """

    def __init__(self, argument, message=None):
        self.argument = argument
        self.message = message

    def __str__(self):
        ret = "MissingArgumentError: missing argument '{}'".format(self.argument)
        if self.message:
            return ret + ", " + self.message
        return ret
